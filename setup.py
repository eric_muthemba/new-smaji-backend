# coding: utf-8

import sys
from setuptools import setup, find_packages

NAME = "server"
VERSION = "1.0.0"
# To install the library, run the following
#
# python setup.py install
#
# prerequisite: setuptools
# http://pypi.python.org/pypi/setuptools

REQUIRES = ["connexion"]

setup(
    name=NAME,
    version=VERSION,
    description="Smaji Data service Api&#x27;s",
    author_email="eric@diasporaai.com",
    url="",
    keywords=["Swagger", "Smaji Data service Api&#x27;s"],
    install_requires=REQUIRES,
    packages=find_packages(),
    package_data={'': ['swagger/swagger.yaml']},
    include_package_data=True,
    entry_points={
        'console_scripts': ['server=server.__main__:main']},
    long_description=""" smaji data apis  """
)
