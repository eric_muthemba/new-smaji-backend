# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class Manufacturer(Model):
    """

    Do not edit the class manually.
    """
    def __init__(self, registered_name: str=None, business_name: str=None, business_number: str=None, description: str=None, country: str=None, city: str=None, address: str=None):  # noqa: E501
        """Manufacturer - a model defined in Swagger

        :param registered_name: The registered_name of this Manufacturer.  # noqa: E501
        :type registered_name: str
        :param business_name: The business_name of this Manufacturer.  # noqa: E501
        :type business_name: str
        :param business_number: The business_number of this Manufacturer.  # noqa: E501
        :type business_number: str
        :param description: The description of this Manufacturer.  # noqa: E501
        :type description: str
        :param country: The country of this Manufacturer.  # noqa: E501
        :type country: str
        :param city: The city of this Manufacturer.  # noqa: E501
        :type city: str
        :param address: The address of this Manufacturer.  # noqa: E501
        :type address: str
        """
        self.swagger_types = {
            'registered_name': str,
            'business_name': str,
            'business_number': str,
            'description': str,
            'country': str,
            'city': str,
            'address': str
        }

        self.attribute_map = {
            'registered_name': 'registered_name',
            'business_name': 'business_name',
            'business_number': 'business_number',
            'description': 'description',
            'country': 'country',
            'city': 'city',
            'address': 'address'
        }
        self._registered_name = registered_name
        self._business_name = business_name
        self._business_number = business_number
        self._description = description
        self._country = country
        self._city = city
        self._address = address

    @classmethod
    def from_dict(cls, dikt) -> 'Manufacturer':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The manufacturer of this Manufacturer.  # noqa: E501
        :rtype: Manufacturer
        """
        return util.deserialize_model(dikt, cls)

    @property
    def registered_name(self) -> str:
        """Gets the registered_name of this Manufacturer.


        :return: The registered_name of this Manufacturer.
        :rtype: str
        """
        return self._registered_name

    @registered_name.setter
    def registered_name(self, registered_name: str):
        """Sets the registered_name of this Manufacturer.


        :param registered_name: The registered_name of this Manufacturer.
        :type registered_name: str
        """

        self._registered_name = registered_name

    @property
    def business_name(self) -> str:
        """Gets the business_name of this Manufacturer.


        :return: The business_name of this Manufacturer.
        :rtype: str
        """
        return self._business_name

    @business_name.setter
    def business_name(self, business_name: str):
        """Sets the business_name of this Manufacturer.


        :param business_name: The business_name of this Manufacturer.
        :type business_name: str
        """
        if business_name is None:
            raise ValueError("Invalid value for `business_name`, must not be `None`")  # noqa: E501

        self._business_name = business_name

    @property
    def business_number(self) -> str:
        """Gets the business_number of this Manufacturer.


        :return: The business_number of this Manufacturer.
        :rtype: str
        """
        return self._business_number

    @business_number.setter
    def business_number(self, business_number: str):
        """Sets the business_number of this Manufacturer.


        :param business_number: The business_number of this Manufacturer.
        :type business_number: str
        """

        self._business_number = business_number

    @property
    def description(self) -> str:
        """Gets the description of this Manufacturer.


        :return: The description of this Manufacturer.
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description: str):
        """Sets the description of this Manufacturer.


        :param description: The description of this Manufacturer.
        :type description: str
        """

        self._description = description

    @property
    def country(self) -> str:
        """Gets the country of this Manufacturer.


        :return: The country of this Manufacturer.
        :rtype: str
        """
        return self._country

    @country.setter
    def country(self, country: str):
        """Sets the country of this Manufacturer.


        :param country: The country of this Manufacturer.
        :type country: str
        """

        self._country = country

    @property
    def city(self) -> str:
        """Gets the city of this Manufacturer.


        :return: The city of this Manufacturer.
        :rtype: str
        """
        return self._city

    @city.setter
    def city(self, city: str):
        """Sets the city of this Manufacturer.


        :param city: The city of this Manufacturer.
        :type city: str
        """

        self._city = city

    @property
    def address(self) -> str:
        """Gets the address of this Manufacturer.


        :return: The address of this Manufacturer.
        :rtype: str
        """
        return self._address

    @address.setter
    def address(self, address: str):
        """Sets the address of this Manufacturer.


        :param address: The address of this Manufacturer.
        :type address: str
        """

        self._address = address
