# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class ManufacturerResponse(Model):
    """

    Do not edit the class manually.
    """
    def __init__(self, manufacturer_id: int=None, success: bool=None, message: str=None):  # noqa: E501
        """ManufacturerResponse - a model defined in Swagger

        :param manufacturer_id: The manufacturer_id of this ManufacturerResponse.  # noqa: E501
        :type manufacturer_id: int
        :param success: The success of this ManufacturerResponse.  # noqa: E501
        :type success: bool
        :param message: The message of this ManufacturerResponse.  # noqa: E501
        :type message: str
        """
        self.swagger_types = {
            'manufacturer_id': int,
            'success': bool,
            'message': str
        }

        self.attribute_map = {
            'manufacturer_id': 'manufacturer_id',
            'success': 'success',
            'message': 'message'
        }
        self._manufacturer_id = manufacturer_id
        self._success = success
        self._message = message

    @classmethod
    def from_dict(cls, dikt) -> 'ManufacturerResponse':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The manufacturer_response of this ManufacturerResponse.  # noqa: E501
        :rtype: ManufacturerResponse
        """
        return util.deserialize_model(dikt, cls)

    @property
    def manufacturer_id(self) -> int:
        """Gets the manufacturer_id of this ManufacturerResponse.


        :return: The manufacturer_id of this ManufacturerResponse.
        :rtype: int
        """
        return self._manufacturer_id

    @manufacturer_id.setter
    def manufacturer_id(self, manufacturer_id: int):
        """Sets the manufacturer_id of this ManufacturerResponse.


        :param manufacturer_id: The manufacturer_id of this ManufacturerResponse.
        :type manufacturer_id: int
        """
        if manufacturer_id is None:
            raise ValueError("Invalid value for `manufacturer_id`, must not be `None`")  # noqa: E501

        self._manufacturer_id = manufacturer_id

    @property
    def success(self) -> bool:
        """Gets the success of this ManufacturerResponse.


        :return: The success of this ManufacturerResponse.
        :rtype: bool
        """
        return self._success

    @success.setter
    def success(self, success: bool):
        """Sets the success of this ManufacturerResponse.


        :param success: The success of this ManufacturerResponse.
        :type success: bool
        """
        if success is None:
            raise ValueError("Invalid value for `success`, must not be `None`")  # noqa: E501

        self._success = success

    @property
    def message(self) -> str:
        """Gets the message of this ManufacturerResponse.


        :return: The message of this ManufacturerResponse.
        :rtype: str
        """
        return self._message

    @message.setter
    def message(self, message: str):
        """Sets the message of this ManufacturerResponse.


        :param message: The message of this ManufacturerResponse.
        :type message: str
        """
        if message is None:
            raise ValueError("Invalid value for `message`, must not be `None`")  # noqa: E501

        self._message = message
