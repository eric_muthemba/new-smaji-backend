# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class User(Model):
    """

    Do not edit the class manually.
    """
    def __init__(self, first_name: str=None, last_name: str=None, password: str=None, phone_area_code: str=None, phone_number: str=None, email: str=None):  # noqa: E501
        """User - a model defined in Swagger

        :param first_name: The first_name of this User.  # noqa: E501
        :type first_name: str
        :param last_name: The last_name of this User.  # noqa: E501
        :type last_name: str
        :param password: The password of this User.  # noqa: E501
        :type password: str
        :param phone_area_code: The phone_area_code of this User.  # noqa: E501
        :type phone_area_code: str
        :param phone_number: The phone_number of this User.  # noqa: E501
        :type phone_number: str
        :param email: The email of this User.  # noqa: E501
        :type email: str
        """
        self.swagger_types = {
            'first_name': str,
            'last_name': str,
            'password': str,
            'phone_area_code': str,
            'phone_number': str,
            'email': str
        }

        self.attribute_map = {
            'first_name': 'first_name',
            'last_name': 'last_name',
            'password': 'password',
            'phone_area_code': 'phone_area_code',
            'phone_number': 'phone_number',
            'email': 'email'
        }
        self._first_name = first_name
        self._last_name = last_name
        self._password = password
        self._phone_area_code = phone_area_code
        self._phone_number = phone_number
        self._email = email

    @classmethod
    def from_dict(cls, dikt) -> 'User':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The User of this User.  # noqa: E501
        :rtype: User
        """
        return util.deserialize_model(dikt, cls)

    @property
    def first_name(self) -> str:
        """Gets the first_name of this User.


        :return: The first_name of this User.
        :rtype: str
        """
        return self._first_name

    @first_name.setter
    def first_name(self, first_name: str):
        """Sets the first_name of this User.


        :param first_name: The first_name of this User.
        :type first_name: str
        """
        if first_name is None:
            raise ValueError("Invalid value for `first_name`, must not be `None`")  # noqa: E501

        self._first_name = first_name

    @property
    def last_name(self) -> str:
        """Gets the last_name of this User.


        :return: The last_name of this User.
        :rtype: str
        """
        return self._last_name

    @last_name.setter
    def last_name(self, last_name: str):
        """Sets the last_name of this User.


        :param last_name: The last_name of this User.
        :type last_name: str
        """
        if last_name is None:
            raise ValueError("Invalid value for `last_name`, must not be `None`")  # noqa: E501

        self._last_name = last_name

    @property
    def password(self) -> str:
        """Gets the password of this User.


        :return: The password of this User.
        :rtype: str
        """
        return self._password

    @password.setter
    def password(self, password: str):
        """Sets the password of this User.


        :param password: The password of this User.
        :type password: str
        """
        if password is None:
            raise ValueError("Invalid value for `password`, must not be `None`")  # noqa: E501

        self._password = password

    @property
    def phone_area_code(self) -> str:
        """Gets the phone_area_code of this User.


        :return: The phone_area_code of this User.
        :rtype: str
        """
        return self._phone_area_code

    @phone_area_code.setter
    def phone_area_code(self, phone_area_code: str):
        """Sets the phone_area_code of this User.


        :param phone_area_code: The phone_area_code of this User.
        :type phone_area_code: str
        """
        if phone_area_code is None:
            raise ValueError("Invalid value for `phone_area_code`, must not be `None`")  # noqa: E501

        self._phone_area_code = phone_area_code

    @property
    def phone_number(self) -> str:
        """Gets the phone_number of this User.


        :return: The phone_number of this User.
        :rtype: str
        """
        return self._phone_number

    @phone_number.setter
    def phone_number(self, phone_number: str):
        """Sets the phone_number of this User.


        :param phone_number: The phone_number of this User.
        :type phone_number: str
        """
        if phone_number is None:
            raise ValueError("Invalid value for `phone_number`, must not be `None`")  # noqa: E501

        self._phone_number = phone_number

    @property
    def email(self) -> str:
        """Gets the email of this User.


        :return: The email of this User.
        :rtype: str
        """
        return self._email

    @email.setter
    def email(self, email: str):
        """Sets the email of this User.


        :param email: The email of this User.
        :type email: str
        """
        if email is None:
            raise ValueError("Invalid value for `email`, must not be `None`")  # noqa: E501

        self._email = email
