# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class RegistrationDaySms(Model):
    """

    Do not edit the class manually.
    """
    def __init__(self, text: List[str]=None, numbers: List[str]=None):  # noqa: E501
        """RegistrationDaySms - a model defined in Swagger

        :param text: The text of this RegistrationDaySms.  # noqa: E501
        :type text: List[str]
        :param numbers: The numbers of this RegistrationDaySms.  # noqa: E501
        :type numbers: List[str]
        """
        self.swagger_types = {
            'text': List[str],
            'numbers': List[str]
        }

        self.attribute_map = {
            'text': 'text',
            'numbers': 'numbers'
        }
        self._text = text
        self._numbers = numbers

    @classmethod
    def from_dict(cls, dikt) -> 'RegistrationDaySms':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The registration_day_sms of this RegistrationDaySms.  # noqa: E501
        :rtype: RegistrationDaySms
        """
        return util.deserialize_model(dikt, cls)

    @property
    def text(self) -> List[str]:
        """Gets the text of this RegistrationDaySms.


        :return: The text of this RegistrationDaySms.
        :rtype: List[str]
        """
        return self._text

    @text.setter
    def text(self, text: List[str]):
        """Sets the text of this RegistrationDaySms.


        :param text: The text of this RegistrationDaySms.
        :type text: List[str]
        """

        self._text = text

    @property
    def numbers(self) -> List[str]:
        """Gets the numbers of this RegistrationDaySms.


        :return: The numbers of this RegistrationDaySms.
        :rtype: List[str]
        """
        return self._numbers

    @numbers.setter
    def numbers(self, numbers: List[str]):
        """Sets the numbers of this RegistrationDaySms.


        :param numbers: The numbers of this RegistrationDaySms.
        :type numbers: List[str]
        """

        self._numbers = numbers
