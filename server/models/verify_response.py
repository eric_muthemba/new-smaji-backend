# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class VerifyResponse(Model):
    """

    Do not edit the class manually.
    """
    def __init__(self, success: bool=None, message: str=None):  # noqa: E501
        """VerifyResponse - a model defined in Swagger

        :param success: The success of this VerifyResponse.  # noqa: E501
        :type success: bool
        :param message: The message of this VerifyResponse.  # noqa: E501
        :type message: str
        """
        self.swagger_types = {
            'success': bool,
            'message': str
        }

        self.attribute_map = {
            'success': 'success',
            'message': 'message'
        }
        self._success = success
        self._message = message

    @classmethod
    def from_dict(cls, dikt) -> 'VerifyResponse':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The VerifyResponse of this VerifyResponse.  # noqa: E501
        :rtype: VerifyResponse
        """
        return util.deserialize_model(dikt, cls)

    @property
    def success(self) -> bool:
        """Gets the success of this VerifyResponse.


        :return: The success of this VerifyResponse.
        :rtype: bool
        """
        return self._success

    @success.setter
    def success(self, success: bool):
        """Sets the success of this VerifyResponse.


        :param success: The success of this VerifyResponse.
        :type success: bool
        """
        if success is None:
            raise ValueError("Invalid value for `success`, must not be `None`")  # noqa: E501

        self._success = success

    @property
    def message(self) -> str:
        """Gets the message of this VerifyResponse.


        :return: The message of this VerifyResponse.
        :rtype: str
        """
        return self._message

    @message.setter
    def message(self, message: str):
        """Sets the message of this VerifyResponse.


        :param message: The message of this VerifyResponse.
        :type message: str
        """
        if message is None:
            raise ValueError("Invalid value for `message`, must not be `None`")  # noqa: E501

        self._message = message
