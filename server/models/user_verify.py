# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class UserVerify(Model):
    """

    Do not edit the class manually.
    """
    def __init__(self, code: str=None):  # noqa: E501
        """UserVerify - a model defined in Swagger

        :param code: The code of this UserVerify.  # noqa: E501
        :type code: str
        """
        self.swagger_types = {
            'code': str
        }

        self.attribute_map = {
            'code': 'code'
        }
        self._code = code

    @classmethod
    def from_dict(cls, dikt) -> 'UserVerify':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The UserVerify of this UserVerify.  # noqa: E501
        :rtype: UserVerify
        """
        return util.deserialize_model(dikt, cls)

    @property
    def code(self) -> str:
        """Gets the code of this UserVerify.


        :return: The code of this UserVerify.
        :rtype: str
        """
        return self._code

    @code.setter
    def code(self, code: str):
        """Sets the code of this UserVerify.


        :param code: The code of this UserVerify.
        :type code: str
        """
        if code is None:
            raise ValueError("Invalid value for `code`, must not be `None`")  # noqa: E501

        self._code = code
