# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class LoginResponse(Model):
    """

    Do not edit the class manually.
    """
    def __init__(self, success: bool=None, message: str=None, id: str=None, first_name: str=None, last_name: str=None):  # noqa: E501
        """LoginResponse - a model defined in Swagger

        :param success: The success of this LoginResponse.  # noqa: E501
        :type success: bool
        :param message: The message of this LoginResponse.  # noqa: E501
        :type message: str
        :param id: The id of this LoginResponse.  # noqa: E501
        :type id: str
        :param first_name: The first_name of this LoginResponse.  # noqa: E501
        :type first_name: str
        :param last_name: The last_name of this LoginResponse.  # noqa: E501
        :type last_name: str
        """
        self.swagger_types = {
            'success': bool,
            'message': str,
            'id': str,
            'first_name': str,
            'last_name': str
        }

        self.attribute_map = {
            'success': 'success',
            'message': 'message',
            'id': 'id',
            'first_name': 'first_name',
            'last_name': 'last_name'
        }
        self._success = success
        self._message = message
        self._id = id
        self._first_name = first_name
        self._last_name = last_name

    @classmethod
    def from_dict(cls, dikt) -> 'LoginResponse':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The LoginResponse of this LoginResponse.  # noqa: E501
        :rtype: LoginResponse
        """
        return util.deserialize_model(dikt, cls)

    @property
    def success(self) -> bool:
        """Gets the success of this LoginResponse.


        :return: The success of this LoginResponse.
        :rtype: bool
        """
        return self._success

    @success.setter
    def success(self, success: bool):
        """Sets the success of this LoginResponse.


        :param success: The success of this LoginResponse.
        :type success: bool
        """

        self._success = success

    @property
    def message(self) -> str:
        """Gets the message of this LoginResponse.


        :return: The message of this LoginResponse.
        :rtype: str
        """
        return self._message

    @message.setter
    def message(self, message: str):
        """Sets the message of this LoginResponse.


        :param message: The message of this LoginResponse.
        :type message: str
        """

        self._message = message

    @property
    def id(self) -> str:
        """Gets the id of this LoginResponse.


        :return: The id of this LoginResponse.
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id: str):
        """Sets the id of this LoginResponse.


        :param id: The id of this LoginResponse.
        :type id: str
        """

        self._id = id

    @property
    def first_name(self) -> str:
        """Gets the first_name of this LoginResponse.


        :return: The first_name of this LoginResponse.
        :rtype: str
        """
        return self._first_name

    @first_name.setter
    def first_name(self, first_name: str):
        """Sets the first_name of this LoginResponse.


        :param first_name: The first_name of this LoginResponse.
        :type first_name: str
        """

        self._first_name = first_name

    @property
    def last_name(self) -> str:
        """Gets the last_name of this LoginResponse.


        :return: The last_name of this LoginResponse.
        :rtype: str
        """
        return self._last_name

    @last_name.setter
    def last_name(self, last_name: str):
        """Sets the last_name of this LoginResponse.


        :param last_name: The last_name of this LoginResponse.
        :type last_name: str
        """

        self._last_name = last_name
