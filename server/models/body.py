# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class Body(Model):
    """
    Do not edit the class manually.
    """
    def __init__(self, asset_image: str=None):  # noqa: E501
        """Body - a model defined in Swagger

        :param asset_image: The asset_image of this Body.  # noqa: E501
        :type asset_image: str
        """
        self.swagger_types = {
            'asset_image': str
        }

        self.attribute_map = {
            'asset_image': 'asset_image'
        }
        self._asset_image = asset_image

    @classmethod
    def from_dict(cls, dikt) -> 'Body':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The body of this Body.  # noqa: E501
        :rtype: Body
        """
        return util.deserialize_model(dikt, cls)

    @property
    def asset_image(self) -> str:
        """Gets the asset_image of this Body.


        :return: The asset_image of this Body.
        :rtype: str
        """
        return self._asset_image

    @asset_image.setter
    def asset_image(self, asset_image: str):
        """Sets the asset_image of this Body.


        :param asset_image: The asset_image of this Body.
        :type asset_image: str
        """

        self._asset_image = asset_image
