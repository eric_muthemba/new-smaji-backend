# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class AccountUserActivateRequest(Model):
    """
    Do not edit the class manually.
    """
    def __init__(self, account_id: int=None, user_id: str=None, role_id: int=None, active: bool=None):  # noqa: E501
        """AccountUserActivateRequest - a model defined in Swagger

        :param account_id: The account_id of this AccountUserActivateRequest.  # noqa: E501
        :type account_id: int
        :param user_id: The user_id of this AccountUserActivateRequest.  # noqa: E501
        :type user_id: str
        :param role_id: The role_id of this AccountUserActivateRequest.  # noqa: E501
        :type role_id: int
        :param active: The active of this AccountUserActivateRequest.  # noqa: E501
        :type active: bool
        """
        self.swagger_types = {
            'account_id': int,
            'user_id': str,
            'role_id': int,
            'active': bool
        }

        self.attribute_map = {
            'account_id': 'account_id',
            'user_id': 'user_id',
            'role_id': 'role_id',
            'active': 'active'
        }
        self._account_id = account_id
        self._user_id = user_id
        self._role_id = role_id
        self._active = active

    @classmethod
    def from_dict(cls, dikt) -> 'AccountUserActivateRequest':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The account_user_activate_request of this AccountUserActivateRequest.  # noqa: E501
        :rtype: AccountUserActivateRequest
        """
        return util.deserialize_model(dikt, cls)

    @property
    def account_id(self) -> int:
        """Gets the account_id of this AccountUserActivateRequest.


        :return: The account_id of this AccountUserActivateRequest.
        :rtype: int
        """
        return self._account_id

    @account_id.setter
    def account_id(self, account_id: int):
        """Sets the account_id of this AccountUserActivateRequest.


        :param account_id: The account_id of this AccountUserActivateRequest.
        :type account_id: int
        """
        if account_id is None:
            raise ValueError("Invalid value for `account_id`, must not be `None`")  # noqa: E501

        self._account_id = account_id

    @property
    def user_id(self) -> str:
        """Gets the user_id of this AccountUserActivateRequest.


        :return: The user_id of this AccountUserActivateRequest.
        :rtype: str
        """
        return self._user_id

    @user_id.setter
    def user_id(self, user_id: str):
        """Sets the user_id of this AccountUserActivateRequest.


        :param user_id: The user_id of this AccountUserActivateRequest.
        :type user_id: str
        """
        if user_id is None:
            raise ValueError("Invalid value for `user_id`, must not be `None`")  # noqa: E501

        self._user_id = user_id

    @property
    def role_id(self) -> int:
        """Gets the role_id of this AccountUserActivateRequest.


        :return: The role_id of this AccountUserActivateRequest.
        :rtype: int
        """
        return self._role_id

    @role_id.setter
    def role_id(self, role_id: int):
        """Sets the role_id of this AccountUserActivateRequest.


        :param role_id: The role_id of this AccountUserActivateRequest.
        :type role_id: int
        """
        if role_id is None:
            raise ValueError("Invalid value for `role_id`, must not be `None`")  # noqa: E501

        self._role_id = role_id

    @property
    def active(self) -> bool:
        """Gets the active of this AccountUserActivateRequest.


        :return: The active of this AccountUserActivateRequest.
        :rtype: bool
        """
        return self._active

    @active.setter
    def active(self, active: bool):
        """Sets the active of this AccountUserActivateRequest.


        :param active: The active of this AccountUserActivateRequest.
        :type active: bool
        """
        if active is None:
            raise ValueError("Invalid value for `active`, must not be `None`")  # noqa: E501

        self._active = active
