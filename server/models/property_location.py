# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class PropertyLocation(Model):
    """

    Do not edit the class manually.
    """
    def __init__(self, coordinates: str=None, number_of_tenants: str=None, number_of_tanks: str=None, property_name: str=None):  # noqa: E501
        """PropertyLocation - a model defined in Swagger

        :param coordinates: The coordinates of this PropertyLocation.  # noqa: E501
        :type coordinates: str
        :param number_of_tenants: The number_of_tenants of this PropertyLocation.  # noqa: E501
        :type number_of_tenants: str
        :param number_of_tanks: The number_of_tanks of this PropertyLocation.  # noqa: E501
        :type number_of_tanks: str
        :param property_name: The property_name of this PropertyLocation.  # noqa: E501
        :type property_name: str
        """
        self.swagger_types = {
            'coordinates': str,
            'number_of_tenants': str,
            'number_of_tanks': str,
            'property_name': str
        }

        self.attribute_map = {
            'coordinates': 'coordinates',
            'number_of_tenants': 'number_of_tenants',
            'number_of_tanks': 'number_of_tanks',
            'property_name': 'property_name'
        }
        self._coordinates = coordinates
        self._number_of_tenants = number_of_tenants
        self._number_of_tanks = number_of_tanks
        self._property_name = property_name

    @classmethod
    def from_dict(cls, dikt) -> 'PropertyLocation':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The property_location of this PropertyLocation.  # noqa: E501
        :rtype: PropertyLocation
        """
        return util.deserialize_model(dikt, cls)

    @property
    def coordinates(self) -> str:
        """Gets the coordinates of this PropertyLocation.


        :return: The coordinates of this PropertyLocation.
        :rtype: str
        """
        return self._coordinates

    @coordinates.setter
    def coordinates(self, coordinates: str):
        """Sets the coordinates of this PropertyLocation.


        :param coordinates: The coordinates of this PropertyLocation.
        :type coordinates: str
        """
        if coordinates is None:
            raise ValueError("Invalid value for `coordinates`, must not be `None`")  # noqa: E501

        self._coordinates = coordinates

    @property
    def number_of_tenants(self) -> str:
        """Gets the number_of_tenants of this PropertyLocation.


        :return: The number_of_tenants of this PropertyLocation.
        :rtype: str
        """
        return self._number_of_tenants

    @number_of_tenants.setter
    def number_of_tenants(self, number_of_tenants: str):
        """Sets the number_of_tenants of this PropertyLocation.


        :param number_of_tenants: The number_of_tenants of this PropertyLocation.
        :type number_of_tenants: str
        """
        if number_of_tenants is None:
            raise ValueError("Invalid value for `number_of_tenants`, must not be `None`")  # noqa: E501

        self._number_of_tenants = number_of_tenants

    @property
    def number_of_tanks(self) -> str:
        """Gets the number_of_tanks of this PropertyLocation.


        :return: The number_of_tanks of this PropertyLocation.
        :rtype: str
        """
        return self._number_of_tanks

    @number_of_tanks.setter
    def number_of_tanks(self, number_of_tanks: str):
        """Sets the number_of_tanks of this PropertyLocation.


        :param number_of_tanks: The number_of_tanks of this PropertyLocation.
        :type number_of_tanks: str
        """
        if number_of_tanks is None:
            raise ValueError("Invalid value for `number_of_tanks`, must not be `None`")  # noqa: E501

        self._number_of_tanks = number_of_tanks

    @property
    def property_name(self) -> str:
        """Gets the property_name of this PropertyLocation.


        :return: The property_name of this PropertyLocation.
        :rtype: str
        """
        return self._property_name

    @property_name.setter
    def property_name(self, property_name: str):
        """Sets the property_name of this PropertyLocation.


        :param property_name: The property_name of this PropertyLocation.
        :type property_name: str
        """

        self._property_name = property_name
