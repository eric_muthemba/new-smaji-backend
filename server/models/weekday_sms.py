# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class WeekdaySms(Model):
    """

    Do not edit the class manually.
    """
    def __init__(self, day_of_the_week: str=None, hour: str=None, minute: str=None):  # noqa: E501
        """WeekdaySms - a model defined in Swagger

        :param day_of_the_week: The day_of_the_week of this WeekdaySms.  # noqa: E501
        :type day_of_the_week: str
        :param hour: The hour of this WeekdaySms.  # noqa: E501
        :type hour: str
        :param minute: The minute of this WeekdaySms.  # noqa: E501
        :type minute: str
        """
        self.swagger_types = {
            'day_of_the_week': str,
            'hour': str,
            'minute': str
        }

        self.attribute_map = {
            'day_of_the_week': 'day_of_the_week',
            'hour': 'hour',
            'minute': 'minute'
        }
        self._day_of_the_week = day_of_the_week
        self._hour = hour
        self._minute = minute

    @classmethod
    def from_dict(cls, dikt) -> 'WeekdaySms':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The weekday_sms of this WeekdaySms.  # noqa: E501
        :rtype: WeekdaySms
        """
        return util.deserialize_model(dikt, cls)

    @property
    def day_of_the_week(self) -> str:
        """Gets the day_of_the_week of this WeekdaySms.


        :return: The day_of_the_week of this WeekdaySms.
        :rtype: str
        """
        return self._day_of_the_week

    @day_of_the_week.setter
    def day_of_the_week(self, day_of_the_week: str):
        """Sets the day_of_the_week of this WeekdaySms.


        :param day_of_the_week: The day_of_the_week of this WeekdaySms.
        :type day_of_the_week: str
        """

        self._day_of_the_week = day_of_the_week

    @property
    def hour(self) -> str:
        """Gets the hour of this WeekdaySms.


        :return: The hour of this WeekdaySms.
        :rtype: str
        """
        return self._hour

    @hour.setter
    def hour(self, hour: str):
        """Sets the hour of this WeekdaySms.


        :param hour: The hour of this WeekdaySms.
        :type hour: str
        """

        self._hour = hour

    @property
    def minute(self) -> str:
        """Gets the minute of this WeekdaySms.


        :return: The minute of this WeekdaySms.
        :rtype: str
        """
        return self._minute

    @minute.setter
    def minute(self, minute: str):
        """Sets the minute of this WeekdaySms.


        :param minute: The minute of this WeekdaySms.
        :type minute: str
        """

        self._minute = minute
