# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class TankRegistration(Model):
    """

    Do not edit the class manually.
    """
    def __init__(self, account_id: int=None, external_id: str=None, group_id: int=None, name: str=None, description: str=None, make_model_id: int=None, serial_number: str=None, manufacture_date: int=None, installation_date: int=None, shape: int=None, material: int=None, color: str=None, placement_position: int=None, height_from_ground_cm: int=None, height_cm: int=None, inlet_height_cm: int=None, outlet_height_cm: int=None, diameter_cm: int=None, length_cm: int=None, width_cm: int=None, weight_full_kg: int=None, weight_empty_kg: int=None, capacity_liters: int=None, water_source: int=None, piping_type: int=None, country: str=None, location: str=None, address: str=None, geographic_coordinates_dd: str=None):  # noqa: E501
        """TankRegistration - a model defined in Swagger

        :param account_id: The account_id of this TankRegistration.  # noqa: E501
        :type account_id: int
        :param external_id: The external_id of this TankRegistration.  # noqa: E501
        :type external_id: str
        :param group_id: The group_id of this TankRegistration.  # noqa: E501
        :type group_id: int
        :param name: The name of this TankRegistration.  # noqa: E501
        :type name: str
        :param description: The description of this TankRegistration.  # noqa: E501
        :type description: str
        :param make_model_id: The make_model_id of this TankRegistration.  # noqa: E501
        :type make_model_id: int
        :param serial_number: The serial_number of this TankRegistration.  # noqa: E501
        :type serial_number: str
        :param manufacture_date: The manufacture_date of this TankRegistration.  # noqa: E501
        :type manufacture_date: int
        :param installation_date: The installation_date of this TankRegistration.  # noqa: E501
        :type installation_date: int
        :param shape: The shape of this TankRegistration.  # noqa: E501
        :type shape: int
        :param material: The material of this TankRegistration.  # noqa: E501
        :type material: int
        :param color: The color of this TankRegistration.  # noqa: E501
        :type color: str
        :param placement_position: The placement_position of this TankRegistration.  # noqa: E501
        :type placement_position: int
        :param height_from_ground_cm: The height_from_ground_cm of this TankRegistration.  # noqa: E501
        :type height_from_ground_cm: int
        :param height_cm: The height_cm of this TankRegistration.  # noqa: E501
        :type height_cm: int
        :param inlet_height_cm: The inlet_height_cm of this TankRegistration.  # noqa: E501
        :type inlet_height_cm: int
        :param outlet_height_cm: The outlet_height_cm of this TankRegistration.  # noqa: E501
        :type outlet_height_cm: int
        :param diameter_cm: The diameter_cm of this TankRegistration.  # noqa: E501
        :type diameter_cm: int
        :param length_cm: The length_cm of this TankRegistration.  # noqa: E501
        :type length_cm: int
        :param width_cm: The width_cm of this TankRegistration.  # noqa: E501
        :type width_cm: int
        :param weight_full_kg: The weight_full_kg of this TankRegistration.  # noqa: E501
        :type weight_full_kg: int
        :param weight_empty_kg: The weight_empty_kg of this TankRegistration.  # noqa: E501
        :type weight_empty_kg: int
        :param capacity_liters: The capacity_liters of this TankRegistration.  # noqa: E501
        :type capacity_liters: int
        :param water_source: The water_source of this TankRegistration.  # noqa: E501
        :type water_source: int
        :param piping_type: The piping_type of this TankRegistration.  # noqa: E501
        :type piping_type: int
        :param country: The country of this TankRegistration.  # noqa: E501
        :type country: str
        :param location: The location of this TankRegistration.  # noqa: E501
        :type location: str
        :param address: The address of this TankRegistration.  # noqa: E501
        :type address: str
        :param geographic_coordinates_dd: The geographic_coordinates_dd of this TankRegistration.  # noqa: E501
        :type geographic_coordinates_dd: str
        """
        self.swagger_types = {
            'account_id': int,
            'external_id': str,
            'group_id': int,
            'name': str,
            'description': str,
            'make_model_id': int,
            'serial_number': str,
            'manufacture_date': int,
            'installation_date': int,
            'shape': int,
            'material': int,
            'color': str,
            'placement_position': int,
            'height_from_ground_cm': int,
            'height_cm': int,
            'inlet_height_cm': int,
            'outlet_height_cm': int,
            'diameter_cm': int,
            'length_cm': int,
            'width_cm': int,
            'weight_full_kg': int,
            'weight_empty_kg': int,
            'capacity_liters': int,
            'water_source': int,
            'piping_type': int,
            'country': str,
            'location': str,
            'address': str,
            'geographic_coordinates_dd': str
        }

        self.attribute_map = {
            'account_id': 'account_id',
            'external_id': 'external_id',
            'group_id': 'group_id',
            'name': 'name',
            'description': 'description',
            'make_model_id': 'make_model_id',
            'serial_number': 'serial_number',
            'manufacture_date': 'manufacture_date',
            'installation_date': 'installation_date',
            'shape': 'shape',
            'material': 'material',
            'color': 'color',
            'placement_position': 'placement_position',
            'height_from_ground_cm': 'height_from_ground_cm',
            'height_cm': 'height_cm',
            'inlet_height_cm': 'inlet_height_cm',
            'outlet_height_cm': 'outlet_height_cm',
            'diameter_cm': 'diameter_cm',
            'length_cm': 'length_cm',
            'width_cm': 'width_cm',
            'weight_full_kg': 'weight_full_kg',
            'weight_empty_kg': 'weight_empty_kg',
            'capacity_liters': 'capacity_liters',
            'water_source': 'water_source',
            'piping_type': 'piping_type',
            'country': 'country',
            'location': 'location',
            'address': 'address',
            'geographic_coordinates_dd': 'geographic_coordinates_dd'
        }
        self._account_id = account_id
        self._external_id = external_id
        self._group_id = group_id
        self._name = name
        self._description = description
        self._make_model_id = make_model_id
        self._serial_number = serial_number
        self._manufacture_date = manufacture_date
        self._installation_date = installation_date
        self._shape = shape
        self._material = material
        self._color = color
        self._placement_position = placement_position
        self._height_from_ground_cm = height_from_ground_cm
        self._height_cm = height_cm
        self._inlet_height_cm = inlet_height_cm
        self._outlet_height_cm = outlet_height_cm
        self._diameter_cm = diameter_cm
        self._length_cm = length_cm
        self._width_cm = width_cm
        self._weight_full_kg = weight_full_kg
        self._weight_empty_kg = weight_empty_kg
        self._capacity_liters = capacity_liters
        self._water_source = water_source
        self._piping_type = piping_type
        self._country = country
        self._location = location
        self._address = address
        self._geographic_coordinates_dd = geographic_coordinates_dd

    @classmethod
    def from_dict(cls, dikt) -> 'TankRegistration':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The tank_registration of this TankRegistration.  # noqa: E501
        :rtype: TankRegistration
        """
        return util.deserialize_model(dikt, cls)

    @property
    def account_id(self) -> int:
        """Gets the account_id of this TankRegistration.


        :return: The account_id of this TankRegistration.
        :rtype: int
        """
        return self._account_id

    @account_id.setter
    def account_id(self, account_id: int):
        """Sets the account_id of this TankRegistration.


        :param account_id: The account_id of this TankRegistration.
        :type account_id: int
        """
        if account_id is None:
            raise ValueError("Invalid value for `account_id`, must not be `None`")  # noqa: E501

        self._account_id = account_id

    @property
    def external_id(self) -> str:
        """Gets the external_id of this TankRegistration.


        :return: The external_id of this TankRegistration.
        :rtype: str
        """
        return self._external_id

    @external_id.setter
    def external_id(self, external_id: str):
        """Sets the external_id of this TankRegistration.


        :param external_id: The external_id of this TankRegistration.
        :type external_id: str
        """

        self._external_id = external_id

    @property
    def group_id(self) -> int:
        """Gets the group_id of this TankRegistration.


        :return: The group_id of this TankRegistration.
        :rtype: int
        """
        return self._group_id

    @group_id.setter
    def group_id(self, group_id: int):
        """Sets the group_id of this TankRegistration.


        :param group_id: The group_id of this TankRegistration.
        :type group_id: int
        """

        self._group_id = group_id

    @property
    def name(self) -> str:
        """Gets the name of this TankRegistration.


        :return: The name of this TankRegistration.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name: str):
        """Sets the name of this TankRegistration.


        :param name: The name of this TankRegistration.
        :type name: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    @property
    def description(self) -> str:
        """Gets the description of this TankRegistration.


        :return: The description of this TankRegistration.
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description: str):
        """Sets the description of this TankRegistration.


        :param description: The description of this TankRegistration.
        :type description: str
        """

        self._description = description

    @property
    def make_model_id(self) -> int:
        """Gets the make_model_id of this TankRegistration.


        :return: The make_model_id of this TankRegistration.
        :rtype: int
        """
        return self._make_model_id

    @make_model_id.setter
    def make_model_id(self, make_model_id: int):
        """Sets the make_model_id of this TankRegistration.


        :param make_model_id: The make_model_id of this TankRegistration.
        :type make_model_id: int
        """

        self._make_model_id = make_model_id

    @property
    def serial_number(self) -> str:
        """Gets the serial_number of this TankRegistration.


        :return: The serial_number of this TankRegistration.
        :rtype: str
        """
        return self._serial_number

    @serial_number.setter
    def serial_number(self, serial_number: str):
        """Sets the serial_number of this TankRegistration.


        :param serial_number: The serial_number of this TankRegistration.
        :type serial_number: str
        """

        self._serial_number = serial_number

    @property
    def manufacture_date(self) -> int:
        """Gets the manufacture_date of this TankRegistration.


        :return: The manufacture_date of this TankRegistration.
        :rtype: int
        """
        return self._manufacture_date

    @manufacture_date.setter
    def manufacture_date(self, manufacture_date: int):
        """Sets the manufacture_date of this TankRegistration.


        :param manufacture_date: The manufacture_date of this TankRegistration.
        :type manufacture_date: int
        """

        self._manufacture_date = manufacture_date

    @property
    def installation_date(self) -> int:
        """Gets the installation_date of this TankRegistration.


        :return: The installation_date of this TankRegistration.
        :rtype: int
        """
        return self._installation_date

    @installation_date.setter
    def installation_date(self, installation_date: int):
        """Sets the installation_date of this TankRegistration.


        :param installation_date: The installation_date of this TankRegistration.
        :type installation_date: int
        """

        self._installation_date = installation_date

    @property
    def shape(self) -> int:
        """Gets the shape of this TankRegistration.


        :return: The shape of this TankRegistration.
        :rtype: int
        """
        return self._shape

    @shape.setter
    def shape(self, shape: int):
        """Sets the shape of this TankRegistration.


        :param shape: The shape of this TankRegistration.
        :type shape: int
        """

        self._shape = shape

    @property
    def material(self) -> int:
        """Gets the material of this TankRegistration.


        :return: The material of this TankRegistration.
        :rtype: int
        """
        return self._material

    @material.setter
    def material(self, material: int):
        """Sets the material of this TankRegistration.


        :param material: The material of this TankRegistration.
        :type material: int
        """

        self._material = material

    @property
    def color(self) -> str:
        """Gets the color of this TankRegistration.


        :return: The color of this TankRegistration.
        :rtype: str
        """
        return self._color

    @color.setter
    def color(self, color: str):
        """Sets the color of this TankRegistration.


        :param color: The color of this TankRegistration.
        :type color: str
        """

        self._color = color

    @property
    def placement_position(self) -> int:
        """Gets the placement_position of this TankRegistration.


        :return: The placement_position of this TankRegistration.
        :rtype: int
        """
        return self._placement_position

    @placement_position.setter
    def placement_position(self, placement_position: int):
        """Sets the placement_position of this TankRegistration.


        :param placement_position: The placement_position of this TankRegistration.
        :type placement_position: int
        """
        if placement_position is None:
            raise ValueError("Invalid value for `placement_position`, must not be `None`")  # noqa: E501

        self._placement_position = placement_position

    @property
    def height_from_ground_cm(self) -> int:
        """Gets the height_from_ground_cm of this TankRegistration.


        :return: The height_from_ground_cm of this TankRegistration.
        :rtype: int
        """
        return self._height_from_ground_cm

    @height_from_ground_cm.setter
    def height_from_ground_cm(self, height_from_ground_cm: int):
        """Sets the height_from_ground_cm of this TankRegistration.


        :param height_from_ground_cm: The height_from_ground_cm of this TankRegistration.
        :type height_from_ground_cm: int
        """

        self._height_from_ground_cm = height_from_ground_cm

    @property
    def height_cm(self) -> int:
        """Gets the height_cm of this TankRegistration.


        :return: The height_cm of this TankRegistration.
        :rtype: int
        """
        return self._height_cm

    @height_cm.setter
    def height_cm(self, height_cm: int):
        """Sets the height_cm of this TankRegistration.


        :param height_cm: The height_cm of this TankRegistration.
        :type height_cm: int
        """
        if height_cm is None:
            raise ValueError("Invalid value for `height_cm`, must not be `None`")  # noqa: E501

        self._height_cm = height_cm

    @property
    def inlet_height_cm(self) -> int:
        """Gets the inlet_height_cm of this TankRegistration.


        :return: The inlet_height_cm of this TankRegistration.
        :rtype: int
        """
        return self._inlet_height_cm

    @inlet_height_cm.setter
    def inlet_height_cm(self, inlet_height_cm: int):
        """Sets the inlet_height_cm of this TankRegistration.


        :param inlet_height_cm: The inlet_height_cm of this TankRegistration.
        :type inlet_height_cm: int
        """

        self._inlet_height_cm = inlet_height_cm

    @property
    def outlet_height_cm(self) -> int:
        """Gets the outlet_height_cm of this TankRegistration.


        :return: The outlet_height_cm of this TankRegistration.
        :rtype: int
        """
        return self._outlet_height_cm

    @outlet_height_cm.setter
    def outlet_height_cm(self, outlet_height_cm: int):
        """Sets the outlet_height_cm of this TankRegistration.


        :param outlet_height_cm: The outlet_height_cm of this TankRegistration.
        :type outlet_height_cm: int
        """

        self._outlet_height_cm = outlet_height_cm

    @property
    def diameter_cm(self) -> int:
        """Gets the diameter_cm of this TankRegistration.


        :return: The diameter_cm of this TankRegistration.
        :rtype: int
        """
        return self._diameter_cm

    @diameter_cm.setter
    def diameter_cm(self, diameter_cm: int):
        """Sets the diameter_cm of this TankRegistration.


        :param diameter_cm: The diameter_cm of this TankRegistration.
        :type diameter_cm: int
        """

        self._diameter_cm = diameter_cm

    @property
    def length_cm(self) -> int:
        """Gets the length_cm of this TankRegistration.


        :return: The length_cm of this TankRegistration.
        :rtype: int
        """
        return self._length_cm

    @length_cm.setter
    def length_cm(self, length_cm: int):
        """Sets the length_cm of this TankRegistration.


        :param length_cm: The length_cm of this TankRegistration.
        :type length_cm: int
        """

        self._length_cm = length_cm

    @property
    def width_cm(self) -> int:
        """Gets the width_cm of this TankRegistration.


        :return: The width_cm of this TankRegistration.
        :rtype: int
        """
        return self._width_cm

    @width_cm.setter
    def width_cm(self, width_cm: int):
        """Sets the width_cm of this TankRegistration.


        :param width_cm: The width_cm of this TankRegistration.
        :type width_cm: int
        """

        self._width_cm = width_cm

    @property
    def weight_full_kg(self) -> int:
        """Gets the weight_full_kg of this TankRegistration.


        :return: The weight_full_kg of this TankRegistration.
        :rtype: int
        """
        return self._weight_full_kg

    @weight_full_kg.setter
    def weight_full_kg(self, weight_full_kg: int):
        """Sets the weight_full_kg of this TankRegistration.


        :param weight_full_kg: The weight_full_kg of this TankRegistration.
        :type weight_full_kg: int
        """

        self._weight_full_kg = weight_full_kg

    @property
    def weight_empty_kg(self) -> int:
        """Gets the weight_empty_kg of this TankRegistration.


        :return: The weight_empty_kg of this TankRegistration.
        :rtype: int
        """
        return self._weight_empty_kg

    @weight_empty_kg.setter
    def weight_empty_kg(self, weight_empty_kg: int):
        """Sets the weight_empty_kg of this TankRegistration.


        :param weight_empty_kg: The weight_empty_kg of this TankRegistration.
        :type weight_empty_kg: int
        """

        self._weight_empty_kg = weight_empty_kg

    @property
    def capacity_liters(self) -> int:
        """Gets the capacity_liters of this TankRegistration.


        :return: The capacity_liters of this TankRegistration.
        :rtype: int
        """
        return self._capacity_liters

    @capacity_liters.setter
    def capacity_liters(self, capacity_liters: int):
        """Sets the capacity_liters of this TankRegistration.


        :param capacity_liters: The capacity_liters of this TankRegistration.
        :type capacity_liters: int
        """
        if capacity_liters is None:
            raise ValueError("Invalid value for `capacity_liters`, must not be `None`")  # noqa: E501

        self._capacity_liters = capacity_liters

    @property
    def water_source(self) -> int:
        """Gets the water_source of this TankRegistration.


        :return: The water_source of this TankRegistration.
        :rtype: int
        """
        return self._water_source

    @water_source.setter
    def water_source(self, water_source: int):
        """Sets the water_source of this TankRegistration.


        :param water_source: The water_source of this TankRegistration.
        :type water_source: int
        """

        self._water_source = water_source

    @property
    def piping_type(self) -> int:
        """Gets the piping_type of this TankRegistration.


        :return: The piping_type of this TankRegistration.
        :rtype: int
        """
        return self._piping_type

    @piping_type.setter
    def piping_type(self, piping_type: int):
        """Sets the piping_type of this TankRegistration.


        :param piping_type: The piping_type of this TankRegistration.
        :type piping_type: int
        """

        self._piping_type = piping_type

    @property
    def country(self) -> str:
        """Gets the country of this TankRegistration.


        :return: The country of this TankRegistration.
        :rtype: str
        """
        return self._country

    @country.setter
    def country(self, country: str):
        """Sets the country of this TankRegistration.


        :param country: The country of this TankRegistration.
        :type country: str
        """

        self._country = country

    @property
    def location(self) -> str:
        """Gets the location of this TankRegistration.


        :return: The location of this TankRegistration.
        :rtype: str
        """
        return self._location

    @location.setter
    def location(self, location: str):
        """Sets the location of this TankRegistration.


        :param location: The location of this TankRegistration.
        :type location: str
        """

        self._location = location

    @property
    def address(self) -> str:
        """Gets the address of this TankRegistration.


        :return: The address of this TankRegistration.
        :rtype: str
        """
        return self._address

    @address.setter
    def address(self, address: str):
        """Sets the address of this TankRegistration.


        :param address: The address of this TankRegistration.
        :type address: str
        """

        self._address = address

    @property
    def geographic_coordinates_dd(self) -> str:
        """Gets the geographic_coordinates_dd of this TankRegistration.


        :return: The geographic_coordinates_dd of this TankRegistration.
        :rtype: str
        """
        return self._geographic_coordinates_dd

    @geographic_coordinates_dd.setter
    def geographic_coordinates_dd(self, geographic_coordinates_dd: str):
        """Sets the geographic_coordinates_dd of this TankRegistration.


        :param geographic_coordinates_dd: The geographic_coordinates_dd of this TankRegistration.
        :type geographic_coordinates_dd: str
        """

        self._geographic_coordinates_dd = geographic_coordinates_dd
