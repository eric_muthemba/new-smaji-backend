# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class Push(Model):
    """

    Do not edit the class manually.
    """
    def __init__(self, device_id: str=None, data: object=None):  # noqa: E501
        """Push - a model defined in Swagger

        :param device_id: The device_id of this Push.  # noqa: E501
        :type device_id: str
        :param data: The data of this Push.  # noqa: E501
        :type data: object
        """
        self.swagger_types = {
            'device_id': str,
            'data': object
        }

        self.attribute_map = {
            'device_id': 'Device_id',
            'data': 'Data'
        }
        self._device_id = device_id
        self._data = data

    @classmethod
    def from_dict(cls, dikt) -> 'Push':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The Push of this Push.  # noqa: E501
        :rtype: Push
        """
        return util.deserialize_model(dikt, cls)

    @property
    def device_id(self) -> str:
        """Gets the device_id of this Push.


        :return: The device_id of this Push.
        :rtype: str
        """
        return self._device_id

    @device_id.setter
    def device_id(self, device_id: str):
        """Sets the device_id of this Push.


        :param device_id: The device_id of this Push.
        :type device_id: str
        """
        if device_id is None:
            raise ValueError("Invalid value for `device_id`, must not be `None`")  # noqa: E501

        self._device_id = device_id

    @property
    def data(self) -> object:
        """Gets the data of this Push.


        :return: The data of this Push.
        :rtype: object
        """
        return self._data

    @data.setter
    def data(self, data: object):
        """Sets the data of this Push.


        :param data: The data of this Push.
        :type data: object
        """
        if data is None:
            raise ValueError("Invalid value for `data`, must not be `None`")  # noqa: E501

        self._data = data
