# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class TwoFactorAuthenticationResponse(Model):
    """

    Do not edit the class manually.
    """
    def __init__(self, success: bool=None, message: str=None, sent_authentication_code: bool=None):  # noqa: E501
        """TwoFactorAuthenticationResponse - a model defined in Swagger

        :param success: The success of this TwoFactorAuthenticationResponse.  # noqa: E501
        :type success: bool
        :param message: The message of this TwoFactorAuthenticationResponse.  # noqa: E501
        :type message: str
        :param sent_authentication_code: The sent_authentication_code of this TwoFactorAuthenticationResponse.  # noqa: E501
        :type sent_authentication_code: bool
        """
        self.swagger_types = {
            'success': bool,
            'message': str,
            'sent_authentication_code': bool
        }

        self.attribute_map = {
            'success': 'success',
            'message': 'message',
            'sent_authentication_code': 'sent_authentication_code'
        }
        self._success = success
        self._message = message
        self._sent_authentication_code = sent_authentication_code

    @classmethod
    def from_dict(cls, dikt) -> 'TwoFactorAuthenticationResponse':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The TwoFactorAuthenticationResponse of this TwoFactorAuthenticationResponse.  # noqa: E501
        :rtype: TwoFactorAuthenticationResponse
        """
        return util.deserialize_model(dikt, cls)

    @property
    def success(self) -> bool:
        """Gets the success of this TwoFactorAuthenticationResponse.


        :return: The success of this TwoFactorAuthenticationResponse.
        :rtype: bool
        """
        return self._success

    @success.setter
    def success(self, success: bool):
        """Sets the success of this TwoFactorAuthenticationResponse.


        :param success: The success of this TwoFactorAuthenticationResponse.
        :type success: bool
        """

        self._success = success

    @property
    def message(self) -> str:
        """Gets the message of this TwoFactorAuthenticationResponse.


        :return: The message of this TwoFactorAuthenticationResponse.
        :rtype: str
        """
        return self._message

    @message.setter
    def message(self, message: str):
        """Sets the message of this TwoFactorAuthenticationResponse.


        :param message: The message of this TwoFactorAuthenticationResponse.
        :type message: str
        """

        self._message = message

    @property
    def sent_authentication_code(self) -> bool:
        """Gets the sent_authentication_code of this TwoFactorAuthenticationResponse.


        :return: The sent_authentication_code of this TwoFactorAuthenticationResponse.
        :rtype: bool
        """
        return self._sent_authentication_code

    @sent_authentication_code.setter
    def sent_authentication_code(self, sent_authentication_code: bool):
        """Sets the sent_authentication_code of this TwoFactorAuthenticationResponse.


        :param sent_authentication_code: The sent_authentication_code of this TwoFactorAuthenticationResponse.
        :type sent_authentication_code: bool
        """

        self._sent_authentication_code = sent_authentication_code
