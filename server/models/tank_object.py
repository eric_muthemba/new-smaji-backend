# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class TankObject(Model):
    """

    Do not edit the class manually.
    """
    def __init__(self, org_id: int=None, user_id: int=None, elevation: str=None, relative_location: str=None, shape: str=None, height: int=None, width: int=None, volume: int=None, length: int=None, condition: str=None, location_on_property: str=None, image_url: str=None, comment: str=None, is_uploaded: int=None):  # noqa: E501
        """TankObject - a model defined in Swagger

        :param org_id: The org_id of this TankObject.  # noqa: E501
        :type org_id: int
        :param user_id: The user_id of this TankObject.  # noqa: E501
        :type user_id: int
        :param elevation: The elevation of this TankObject.  # noqa: E501
        :type elevation: str
        :param relative_location: The relative_location of this TankObject.  # noqa: E501
        :type relative_location: str
        :param shape: The shape of this TankObject.  # noqa: E501
        :type shape: str
        :param height: The height of this TankObject.  # noqa: E501
        :type height: int
        :param width: The width of this TankObject.  # noqa: E501
        :type width: int
        :param volume: The volume of this TankObject.  # noqa: E501
        :type volume: int
        :param length: The length of this TankObject.  # noqa: E501
        :type length: int
        :param condition: The condition of this TankObject.  # noqa: E501
        :type condition: str
        :param location_on_property: The location_on_property of this TankObject.  # noqa: E501
        :type location_on_property: str
        :param image_url: The image_url of this TankObject.  # noqa: E501
        :type image_url: str
        :param comment: The comment of this TankObject.  # noqa: E501
        :type comment: str
        :param is_uploaded: The is_uploaded of this TankObject.  # noqa: E501
        :type is_uploaded: int
        """
        self.swagger_types = {
            'org_id': int,
            'user_id': int,
            'elevation': str,
            'relative_location': str,
            'shape': str,
            'height': int,
            'width': int,
            'volume': int,
            'length': int,
            'condition': str,
            'location_on_property': str,
            'image_url': str,
            'comment': str,
            'is_uploaded': int
        }

        self.attribute_map = {
            'org_id': 'org_id',
            'user_id': 'user_id',
            'elevation': 'Elevation',
            'relative_location': 'relative_location',
            'shape': 'shape',
            'height': 'height',
            'width': 'width',
            'volume': 'volume',
            'length': 'length',
            'condition': 'condition',
            'location_on_property': 'location_on_property',
            'image_url': 'image_url',
            'comment': 'Comment',
            'is_uploaded': 'is_uploaded'
        }
        self._org_id = org_id
        self._user_id = user_id
        self._elevation = elevation
        self._relative_location = relative_location
        self._shape = shape
        self._height = height
        self._width = width
        self._volume = volume
        self._length = length
        self._condition = condition
        self._location_on_property = location_on_property
        self._image_url = image_url
        self._comment = comment
        self._is_uploaded = is_uploaded

    @classmethod
    def from_dict(cls, dikt) -> 'TankObject':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The tank_object of this TankObject.  # noqa: E501
        :rtype: TankObject
        """
        return util.deserialize_model(dikt, cls)

    @property
    def org_id(self) -> int:
        """Gets the org_id of this TankObject.


        :return: The org_id of this TankObject.
        :rtype: int
        """
        return self._org_id

    @org_id.setter
    def org_id(self, org_id: int):
        """Sets the org_id of this TankObject.


        :param org_id: The org_id of this TankObject.
        :type org_id: int
        """

        self._org_id = org_id

    @property
    def user_id(self) -> int:
        """Gets the user_id of this TankObject.


        :return: The user_id of this TankObject.
        :rtype: int
        """
        return self._user_id

    @user_id.setter
    def user_id(self, user_id: int):
        """Sets the user_id of this TankObject.


        :param user_id: The user_id of this TankObject.
        :type user_id: int
        """

        self._user_id = user_id

    @property
    def elevation(self) -> str:
        """Gets the elevation of this TankObject.


        :return: The elevation of this TankObject.
        :rtype: str
        """
        return self._elevation

    @elevation.setter
    def elevation(self, elevation: str):
        """Sets the elevation of this TankObject.


        :param elevation: The elevation of this TankObject.
        :type elevation: str
        """

        self._elevation = elevation

    @property
    def relative_location(self) -> str:
        """Gets the relative_location of this TankObject.


        :return: The relative_location of this TankObject.
        :rtype: str
        """
        return self._relative_location

    @relative_location.setter
    def relative_location(self, relative_location: str):
        """Sets the relative_location of this TankObject.


        :param relative_location: The relative_location of this TankObject.
        :type relative_location: str
        """

        self._relative_location = relative_location

    @property
    def shape(self) -> str:
        """Gets the shape of this TankObject.


        :return: The shape of this TankObject.
        :rtype: str
        """
        return self._shape

    @shape.setter
    def shape(self, shape: str):
        """Sets the shape of this TankObject.


        :param shape: The shape of this TankObject.
        :type shape: str
        """

        self._shape = shape

    @property
    def height(self) -> int:
        """Gets the height of this TankObject.


        :return: The height of this TankObject.
        :rtype: int
        """
        return self._height

    @height.setter
    def height(self, height: int):
        """Sets the height of this TankObject.


        :param height: The height of this TankObject.
        :type height: int
        """

        self._height = height

    @property
    def width(self) -> int:
        """Gets the width of this TankObject.


        :return: The width of this TankObject.
        :rtype: int
        """
        return self._width

    @width.setter
    def width(self, width: int):
        """Sets the width of this TankObject.


        :param width: The width of this TankObject.
        :type width: int
        """

        self._width = width

    @property
    def volume(self) -> int:
        """Gets the volume of this TankObject.


        :return: The volume of this TankObject.
        :rtype: int
        """
        return self._volume

    @volume.setter
    def volume(self, volume: int):
        """Sets the volume of this TankObject.


        :param volume: The volume of this TankObject.
        :type volume: int
        """

        self._volume = volume

    @property
    def length(self) -> int:
        """Gets the length of this TankObject.


        :return: The length of this TankObject.
        :rtype: int
        """
        return self._length

    @length.setter
    def length(self, length: int):
        """Sets the length of this TankObject.


        :param length: The length of this TankObject.
        :type length: int
        """

        self._length = length

    @property
    def condition(self) -> str:
        """Gets the condition of this TankObject.


        :return: The condition of this TankObject.
        :rtype: str
        """
        return self._condition

    @condition.setter
    def condition(self, condition: str):
        """Sets the condition of this TankObject.


        :param condition: The condition of this TankObject.
        :type condition: str
        """

        self._condition = condition

    @property
    def location_on_property(self) -> str:
        """Gets the location_on_property of this TankObject.


        :return: The location_on_property of this TankObject.
        :rtype: str
        """
        return self._location_on_property

    @location_on_property.setter
    def location_on_property(self, location_on_property: str):
        """Sets the location_on_property of this TankObject.


        :param location_on_property: The location_on_property of this TankObject.
        :type location_on_property: str
        """

        self._location_on_property = location_on_property

    @property
    def image_url(self) -> str:
        """Gets the image_url of this TankObject.


        :return: The image_url of this TankObject.
        :rtype: str
        """
        return self._image_url

    @image_url.setter
    def image_url(self, image_url: str):
        """Sets the image_url of this TankObject.


        :param image_url: The image_url of this TankObject.
        :type image_url: str
        """

        self._image_url = image_url

    @property
    def comment(self) -> str:
        """Gets the comment of this TankObject.


        :return: The comment of this TankObject.
        :rtype: str
        """
        return self._comment

    @comment.setter
    def comment(self, comment: str):
        """Sets the comment of this TankObject.


        :param comment: The comment of this TankObject.
        :type comment: str
        """

        self._comment = comment

    @property
    def is_uploaded(self) -> int:
        """Gets the is_uploaded of this TankObject.


        :return: The is_uploaded of this TankObject.
        :rtype: int
        """
        return self._is_uploaded

    @is_uploaded.setter
    def is_uploaded(self, is_uploaded: int):
        """Sets the is_uploaded of this TankObject.


        :param is_uploaded: The is_uploaded of this TankObject.
        :type is_uploaded: int
        """

        self._is_uploaded = is_uploaded
