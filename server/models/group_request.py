# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class GroupRequest(Model):
    """
    Do not edit the class manually.
    """
    def __init__(self, account_id: int=None, name: str=None, description: str=None, parent_id: int=None):  # noqa: E501
        """GroupRequest - a model defined in Swagger

        :param account_id: The account_id of this GroupRequest.  # noqa: E501
        :type account_id: int
        :param name: The name of this GroupRequest.  # noqa: E501
        :type name: str
        :param description: The description of this GroupRequest.  # noqa: E501
        :type description: str
        :param parent_id: The parent_id of this GroupRequest.  # noqa: E501
        :type parent_id: int
        """
        self.swagger_types = {
            'account_id': int,
            'name': str,
            'description': str,
            'parent_id': int
        }

        self.attribute_map = {
            'account_id': 'account_id',
            'name': 'name',
            'description': 'description',
            'parent_id': 'parent_id'
        }
        self._account_id = account_id
        self._name = name
        self._description = description
        self._parent_id = parent_id

    @classmethod
    def from_dict(cls, dikt) -> 'GroupRequest':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The group_request of this GroupRequest.  # noqa: E501
        :rtype: GroupRequest
        """
        return util.deserialize_model(dikt, cls)

    @property
    def account_id(self) -> int:
        """Gets the account_id of this GroupRequest.


        :return: The account_id of this GroupRequest.
        :rtype: int
        """
        return self._account_id

    @account_id.setter
    def account_id(self, account_id: int):
        """Sets the account_id of this GroupRequest.


        :param account_id: The account_id of this GroupRequest.
        :type account_id: int
        """
        if account_id is None:
            raise ValueError("Invalid value for `account_id`, must not be `None`")  # noqa: E501

        self._account_id = account_id

    @property
    def name(self) -> str:
        """Gets the name of this GroupRequest.


        :return: The name of this GroupRequest.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name: str):
        """Sets the name of this GroupRequest.


        :param name: The name of this GroupRequest.
        :type name: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    @property
    def description(self) -> str:
        """Gets the description of this GroupRequest.


        :return: The description of this GroupRequest.
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description: str):
        """Sets the description of this GroupRequest.


        :param description: The description of this GroupRequest.
        :type description: str
        """

        self._description = description

    @property
    def parent_id(self) -> int:
        """Gets the parent_id of this GroupRequest.


        :return: The parent_id of this GroupRequest.
        :rtype: int
        """
        return self._parent_id

    @parent_id.setter
    def parent_id(self, parent_id: int):
        """Sets the parent_id of this GroupRequest.


        :param parent_id: The parent_id of this GroupRequest.
        :type parent_id: int
        """

        self._parent_id = parent_id
