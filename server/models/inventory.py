# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class Inventory(Model):
    """
    Do not edit the class manually.
    """
    def __init__(self, user_email: str=None):  # noqa: E501
        """Inventory - a model defined in Swagger

        :param user_email: The user_email of this Inventory.  # noqa: E501
        :type user_email: str
        """
        self.swagger_types = {
            'user_email': str
        }

        self.attribute_map = {
            'user_email': 'User_email'
        }
        self._user_email = user_email

    @classmethod
    def from_dict(cls, dikt) -> 'Inventory':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The inventory of this Inventory.  # noqa: E501
        :rtype: Inventory
        """
        return util.deserialize_model(dikt, cls)

    @property
    def user_email(self) -> str:
        """Gets the user_email of this Inventory.


        :return: The user_email of this Inventory.
        :rtype: str
        """
        return self._user_email

    @user_email.setter
    def user_email(self, user_email: str):
        """Sets the user_email of this Inventory.


        :param user_email: The user_email of this Inventory.
        :type user_email: str
        """

        self._user_email = user_email
