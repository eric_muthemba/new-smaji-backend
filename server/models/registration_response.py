# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class RegistrationResponse(Model):
    """

    Do not edit the class manually.
    """
    def __init__(self, device_id: str=None, success: bool=None, message: str=None):  # noqa: E501
        """RegistrationResponse - a model defined in Swagger

        :param device_id: The device_id of this RegistrationResponse.  # noqa: E501
        :type device_id: str
        :param success: The success of this RegistrationResponse.  # noqa: E501
        :type success: bool
        :param message: The message of this RegistrationResponse.  # noqa: E501
        :type message: str
        """
        self.swagger_types = {
            'device_id': str,
            'success': bool,
            'message': str
        }

        self.attribute_map = {
            'device_id': 'device_id',
            'success': 'success',
            'message': 'message'
        }
        self._device_id = device_id
        self._success = success
        self._message = message

    @classmethod
    def from_dict(cls, dikt) -> 'RegistrationResponse':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The registration_response of this RegistrationResponse.  # noqa: E501
        :rtype: RegistrationResponse
        """
        return util.deserialize_model(dikt, cls)

    @property
    def device_id(self) -> str:
        """Gets the device_id of this RegistrationResponse.


        :return: The device_id of this RegistrationResponse.
        :rtype: str
        """
        return self._device_id

    @device_id.setter
    def device_id(self, device_id: str):
        """Sets the device_id of this RegistrationResponse.


        :param device_id: The device_id of this RegistrationResponse.
        :type device_id: str
        """
        if device_id is None:
            raise ValueError("Invalid value for `device_id`, must not be `None`")  # noqa: E501

        self._device_id = device_id

    @property
    def success(self) -> bool:
        """Gets the success of this RegistrationResponse.


        :return: The success of this RegistrationResponse.
        :rtype: bool
        """
        return self._success

    @success.setter
    def success(self, success: bool):
        """Sets the success of this RegistrationResponse.


        :param success: The success of this RegistrationResponse.
        :type success: bool
        """
        if success is None:
            raise ValueError("Invalid value for `success`, must not be `None`")  # noqa: E501

        self._success = success

    @property
    def message(self) -> str:
        """Gets the message of this RegistrationResponse.


        :return: The message of this RegistrationResponse.
        :rtype: str
        """
        return self._message

    @message.setter
    def message(self, message: str):
        """Sets the message of this RegistrationResponse.


        :param message: The message of this RegistrationResponse.
        :type message: str
        """
        if message is None:
            raise ValueError("Invalid value for `message`, must not be `None`")  # noqa: E501

        self._message = message
