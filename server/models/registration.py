# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class Registration(Model):
    """

    Do not edit the class manually.
    """
    def __init__(self, manufacturer_id: int=None, group_id: int=None, name: str=None, description: str=None, make: str=None, model: str=None, serial_number: str=None, imei_number: str=None, sim_number: str=None, communication_protocol: str=None, bought_on: int=None):  # noqa: E501
        """Registration - a model defined in Swagger

        :param manufacturer_id: The manufacturer_id of this Registration.  # noqa: E501
        :type manufacturer_id: int
        :param group_id: The group_id of this Registration.  # noqa: E501
        :type group_id: int
        :param name: The name of this Registration.  # noqa: E501
        :type name: str
        :param description: The description of this Registration.  # noqa: E501
        :type description: str
        :param make: The make of this Registration.  # noqa: E501
        :type make: str
        :param model: The model of this Registration.  # noqa: E501
        :type model: str
        :param serial_number: The serial_number of this Registration.  # noqa: E501
        :type serial_number: str
        :param imei_number: The imei_number of this Registration.  # noqa: E501
        :type imei_number: str
        :param sim_number: The sim_number of this Registration.  # noqa: E501
        :type sim_number: str
        :param communication_protocol: The communication_protocol of this Registration.  # noqa: E501
        :type communication_protocol: str
        :param bought_on: The bought_on of this Registration.  # noqa: E501
        :type bought_on: int
        """
        self.swagger_types = {
            'manufacturer_id': int,
            'group_id': int,
            'name': str,
            'description': str,
            'make': str,
            'model': str,
            'serial_number': str,
            'imei_number': str,
            'sim_number': str,
            'communication_protocol': str,
            'bought_on': int
        }

        self.attribute_map = {
            'manufacturer_id': 'manufacturer_id',
            'group_id': 'group_id',
            'name': 'name',
            'description': 'description',
            'make': 'make',
            'model': 'model',
            'serial_number': 'serial_number',
            'imei_number': 'imei_number',
            'sim_number': 'sim_number',
            'communication_protocol': 'communication_protocol',
            'bought_on': 'bought_on'
        }
        self._manufacturer_id = manufacturer_id
        self._group_id = group_id
        self._name = name
        self._description = description
        self._make = make
        self._model = model
        self._serial_number = serial_number
        self._imei_number = imei_number
        self._sim_number = sim_number
        self._communication_protocol = communication_protocol
        self._bought_on = bought_on

    @classmethod
    def from_dict(cls, dikt) -> 'Registration':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The registration of this Registration.  # noqa: E501
        :rtype: Registration
        """
        return util.deserialize_model(dikt, cls)

    @property
    def manufacturer_id(self) -> int:
        """Gets the manufacturer_id of this Registration.


        :return: The manufacturer_id of this Registration.
        :rtype: int
        """
        return self._manufacturer_id

    @manufacturer_id.setter
    def manufacturer_id(self, manufacturer_id: int):
        """Sets the manufacturer_id of this Registration.


        :param manufacturer_id: The manufacturer_id of this Registration.
        :type manufacturer_id: int
        """
        if manufacturer_id is None:
            raise ValueError("Invalid value for `manufacturer_id`, must not be `None`")  # noqa: E501

        self._manufacturer_id = manufacturer_id

    @property
    def group_id(self) -> int:
        """Gets the group_id of this Registration.


        :return: The group_id of this Registration.
        :rtype: int
        """
        return self._group_id

    @group_id.setter
    def group_id(self, group_id: int):
        """Sets the group_id of this Registration.


        :param group_id: The group_id of this Registration.
        :type group_id: int
        """

        self._group_id = group_id

    @property
    def name(self) -> str:
        """Gets the name of this Registration.


        :return: The name of this Registration.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name: str):
        """Sets the name of this Registration.


        :param name: The name of this Registration.
        :type name: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    @property
    def description(self) -> str:
        """Gets the description of this Registration.


        :return: The description of this Registration.
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description: str):
        """Sets the description of this Registration.


        :param description: The description of this Registration.
        :type description: str
        """

        self._description = description

    @property
    def make(self) -> str:
        """Gets the make of this Registration.


        :return: The make of this Registration.
        :rtype: str
        """
        return self._make

    @make.setter
    def make(self, make: str):
        """Sets the make of this Registration.


        :param make: The make of this Registration.
        :type make: str
        """
        if make is None:
            raise ValueError("Invalid value for `make`, must not be `None`")  # noqa: E501

        self._make = make

    @property
    def model(self) -> str:
        """Gets the model of this Registration.


        :return: The model of this Registration.
        :rtype: str
        """
        return self._model

    @model.setter
    def model(self, model: str):
        """Sets the model of this Registration.


        :param model: The model of this Registration.
        :type model: str
        """
        if model is None:
            raise ValueError("Invalid value for `model`, must not be `None`")  # noqa: E501

        self._model = model

    @property
    def serial_number(self) -> str:
        """Gets the serial_number of this Registration.


        :return: The serial_number of this Registration.
        :rtype: str
        """
        return self._serial_number

    @serial_number.setter
    def serial_number(self, serial_number: str):
        """Sets the serial_number of this Registration.


        :param serial_number: The serial_number of this Registration.
        :type serial_number: str
        """
        if serial_number is None:
            raise ValueError("Invalid value for `serial_number`, must not be `None`")  # noqa: E501

        self._serial_number = serial_number

    @property
    def imei_number(self) -> str:
        """Gets the imei_number of this Registration.


        :return: The imei_number of this Registration.
        :rtype: str
        """
        return self._imei_number

    @imei_number.setter
    def imei_number(self, imei_number: str):
        """Sets the imei_number of this Registration.


        :param imei_number: The imei_number of this Registration.
        :type imei_number: str
        """

        self._imei_number = imei_number

    @property
    def sim_number(self) -> str:
        """Gets the sim_number of this Registration.


        :return: The sim_number of this Registration.
        :rtype: str
        """
        return self._sim_number

    @sim_number.setter
    def sim_number(self, sim_number: str):
        """Sets the sim_number of this Registration.


        :param sim_number: The sim_number of this Registration.
        :type sim_number: str
        """

        self._sim_number = sim_number

    @property
    def communication_protocol(self) -> str:
        """Gets the communication_protocol of this Registration.


        :return: The communication_protocol of this Registration.
        :rtype: str
        """
        return self._communication_protocol

    @communication_protocol.setter
    def communication_protocol(self, communication_protocol: str):
        """Sets the communication_protocol of this Registration.


        :param communication_protocol: The communication_protocol of this Registration.
        :type communication_protocol: str
        """

        self._communication_protocol = communication_protocol

    @property
    def bought_on(self) -> int:
        """Gets the bought_on of this Registration.


        :return: The bought_on of this Registration.
        :rtype: int
        """
        return self._bought_on

    @bought_on.setter
    def bought_on(self, bought_on: int):
        """Sets the bought_on of this Registration.


        :param bought_on: The bought_on of this Registration.
        :type bought_on: int
        """
        if bought_on is None:
            raise ValueError("Invalid value for `bought_on`, must not be `None`")  # noqa: E501

        self._bought_on = bought_on
