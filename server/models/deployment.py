# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class Deployment(Model):
    """
    Do not edit the class manually.
    """
    def __init__(self, device_id: str=None, tank_id: int=None, deployed_on: int=None, deployed_by: int=None, sensor_offset: int=None):  # noqa: E501
        """Deployment - a model defined in Swagger

        :param device_id: The device_id of this Deployment.  # noqa: E501
        :type device_id: str
        :param tank_id: The tank_id of this Deployment.  # noqa: E501
        :type tank_id: int
        :param deployed_on: The deployed_on of this Deployment.  # noqa: E501
        :type deployed_on: int
        :param deployed_by: The deployed_by of this Deployment.  # noqa: E501
        :type deployed_by: int
        :param sensor_offset: The sensor_offset of this Deployment.  # noqa: E501
        :type sensor_offset: int
        """
        self.swagger_types = {
            'device_id': str,
            'tank_id': int,
            'deployed_on': int,
            'deployed_by': int,
            'sensor_offset': int
        }

        self.attribute_map = {
            'device_id': 'device_id',
            'tank_id': 'tank_id',
            'deployed_on': 'deployed_on',
            'deployed_by': 'deployed_by',
            'sensor_offset': 'sensor_offset'
        }
        self._device_id = device_id
        self._tank_id = tank_id
        self._deployed_on = deployed_on
        self._deployed_by = deployed_by
        self._sensor_offset = sensor_offset

    @classmethod
    def from_dict(cls, dikt) -> 'Deployment':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The Deployment of this Deployment.  # noqa: E501
        :rtype: Deployment
        """
        return util.deserialize_model(dikt, cls)

    @property
    def device_id(self) -> str:
        """Gets the device_id of this Deployment.


        :return: The device_id of this Deployment.
        :rtype: str
        """
        return self._device_id

    @device_id.setter
    def device_id(self, device_id: str):
        """Sets the device_id of this Deployment.


        :param device_id: The device_id of this Deployment.
        :type device_id: str
        """
        if device_id is None:
            raise ValueError("Invalid value for `device_id`, must not be `None`")  # noqa: E501

        self._device_id = device_id

    @property
    def tank_id(self) -> int:
        """Gets the tank_id of this Deployment.


        :return: The tank_id of this Deployment.
        :rtype: int
        """
        return self._tank_id

    @tank_id.setter
    def tank_id(self, tank_id: int):
        """Sets the tank_id of this Deployment.


        :param tank_id: The tank_id of this Deployment.
        :type tank_id: int
        """
        if tank_id is None:
            raise ValueError("Invalid value for `tank_id`, must not be `None`")  # noqa: E501

        self._tank_id = tank_id

    @property
    def deployed_on(self) -> int:
        """Gets the deployed_on of this Deployment.


        :return: The deployed_on of this Deployment.
        :rtype: int
        """
        return self._deployed_on

    @deployed_on.setter
    def deployed_on(self, deployed_on: int):
        """Sets the deployed_on of this Deployment.


        :param deployed_on: The deployed_on of this Deployment.
        :type deployed_on: int
        """
        if deployed_on is None:
            raise ValueError("Invalid value for `deployed_on`, must not be `None`")  # noqa: E501

        self._deployed_on = deployed_on

    @property
    def deployed_by(self) -> int:
        """Gets the deployed_by of this Deployment.


        :return: The deployed_by of this Deployment.
        :rtype: int
        """
        return self._deployed_by

    @deployed_by.setter
    def deployed_by(self, deployed_by: int):
        """Sets the deployed_by of this Deployment.


        :param deployed_by: The deployed_by of this Deployment.
        :type deployed_by: int
        """
        if deployed_by is None:
            raise ValueError("Invalid value for `deployed_by`, must not be `None`")  # noqa: E501

        self._deployed_by = deployed_by

    @property
    def sensor_offset(self) -> int:
        """Gets the sensor_offset of this Deployment.


        :return: The sensor_offset of this Deployment.
        :rtype: int
        """
        return self._sensor_offset

    @sensor_offset.setter
    def sensor_offset(self, sensor_offset: int):
        """Sets the sensor_offset of this Deployment.


        :param sensor_offset: The sensor_offset of this Deployment.
        :type sensor_offset: int
        """
        if sensor_offset is None:
            raise ValueError("Invalid value for `sensor_offset`, must not be `None`")  # noqa: E501

        self._sensor_offset = sensor_offset
