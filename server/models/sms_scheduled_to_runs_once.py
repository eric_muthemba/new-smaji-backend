# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class SmsScheduledToRunsOnce(Model):
    """

    Do not edit the class manually.
    """
    def __init__(self, year: str=None, month: str=None, day: str=None, hour: str=None, minute: str=None):  # noqa: E501
        """SmsScheduledToRunsOnce - a model defined in Swagger

        :param year: The year of this SmsScheduledToRunsOnce.  # noqa: E501
        :type year: str
        :param month: The month of this SmsScheduledToRunsOnce.  # noqa: E501
        :type month: str
        :param day: The day of this SmsScheduledToRunsOnce.  # noqa: E501
        :type day: str
        :param hour: The hour of this SmsScheduledToRunsOnce.  # noqa: E501
        :type hour: str
        :param minute: The minute of this SmsScheduledToRunsOnce.  # noqa: E501
        :type minute: str
        """
        self.swagger_types = {
            'year': str,
            'month': str,
            'day': str,
            'hour': str,
            'minute': str
        }

        self.attribute_map = {
            'year': 'year',
            'month': 'month',
            'day': 'day',
            'hour': 'hour',
            'minute': 'minute'
        }
        self._year = year
        self._month = month
        self._day = day
        self._hour = hour
        self._minute = minute

    @classmethod
    def from_dict(cls, dikt) -> 'SmsScheduledToRunsOnce':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The sms_scheduled_to_runs_once of this SmsScheduledToRunsOnce.  # noqa: E501
        :rtype: SmsScheduledToRunsOnce
        """
        return util.deserialize_model(dikt, cls)

    @property
    def year(self) -> str:
        """Gets the year of this SmsScheduledToRunsOnce.


        :return: The year of this SmsScheduledToRunsOnce.
        :rtype: str
        """
        return self._year

    @year.setter
    def year(self, year: str):
        """Sets the year of this SmsScheduledToRunsOnce.


        :param year: The year of this SmsScheduledToRunsOnce.
        :type year: str
        """

        self._year = year

    @property
    def month(self) -> str:
        """Gets the month of this SmsScheduledToRunsOnce.


        :return: The month of this SmsScheduledToRunsOnce.
        :rtype: str
        """
        return self._month

    @month.setter
    def month(self, month: str):
        """Sets the month of this SmsScheduledToRunsOnce.


        :param month: The month of this SmsScheduledToRunsOnce.
        :type month: str
        """

        self._month = month

    @property
    def day(self) -> str:
        """Gets the day of this SmsScheduledToRunsOnce.


        :return: The day of this SmsScheduledToRunsOnce.
        :rtype: str
        """
        return self._day

    @day.setter
    def day(self, day: str):
        """Sets the day of this SmsScheduledToRunsOnce.


        :param day: The day of this SmsScheduledToRunsOnce.
        :type day: str
        """

        self._day = day

    @property
    def hour(self) -> str:
        """Gets the hour of this SmsScheduledToRunsOnce.


        :return: The hour of this SmsScheduledToRunsOnce.
        :rtype: str
        """
        return self._hour

    @hour.setter
    def hour(self, hour: str):
        """Sets the hour of this SmsScheduledToRunsOnce.


        :param hour: The hour of this SmsScheduledToRunsOnce.
        :type hour: str
        """

        self._hour = hour

    @property
    def minute(self) -> str:
        """Gets the minute of this SmsScheduledToRunsOnce.


        :return: The minute of this SmsScheduledToRunsOnce.
        :rtype: str
        """
        return self._minute

    @minute.setter
    def minute(self, minute: str):
        """Sets the minute of this SmsScheduledToRunsOnce.


        :param minute: The minute of this SmsScheduledToRunsOnce.
        :type minute: str
        """

        self._minute = minute
