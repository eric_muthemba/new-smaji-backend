# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class UserResponse(Model):
    """

    Do not edit the class manually.
    """
    def __init__(self, success: bool=None, sent_authentication_code: bool=None, message: str=None):  # noqa: E501
        """UserResponse - a model defined in Swagger

        :param success: The success of this UserResponse.  # noqa: E501
        :type success: bool
        :param sent_authentication_code: The sent_authentication_code of this UserResponse.  # noqa: E501
        :type sent_authentication_code: bool
        :param message: The message of this UserResponse.  # noqa: E501
        :type message: str
        """
        self.swagger_types = {
            'success': bool,
            'sent_authentication_code': bool,
            'message': str
        }

        self.attribute_map = {
            'success': 'success',
            'sent_authentication_code': 'sent_authentication_code',
            'message': 'message'
        }
        self._success = success
        self._sent_authentication_code = sent_authentication_code
        self._message = message

    @classmethod
    def from_dict(cls, dikt) -> 'UserResponse':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The UserResponse of this UserResponse.  # noqa: E501
        :rtype: UserResponse
        """
        return util.deserialize_model(dikt, cls)

    @property
    def success(self) -> bool:
        """Gets the success of this UserResponse.


        :return: The success of this UserResponse.
        :rtype: bool
        """
        return self._success

    @success.setter
    def success(self, success: bool):
        """Sets the success of this UserResponse.


        :param success: The success of this UserResponse.
        :type success: bool
        """
        if success is None:
            raise ValueError("Invalid value for `success`, must not be `None`")  # noqa: E501

        self._success = success

    @property
    def sent_authentication_code(self) -> bool:
        """Gets the sent_authentication_code of this UserResponse.


        :return: The sent_authentication_code of this UserResponse.
        :rtype: bool
        """
        return self._sent_authentication_code

    @sent_authentication_code.setter
    def sent_authentication_code(self, sent_authentication_code: bool):
        """Sets the sent_authentication_code of this UserResponse.


        :param sent_authentication_code: The sent_authentication_code of this UserResponse.
        :type sent_authentication_code: bool
        """
        if sent_authentication_code is None:
            raise ValueError("Invalid value for `sent_authentication_code`, must not be `None`")  # noqa: E501

        self._sent_authentication_code = sent_authentication_code

    @property
    def message(self) -> str:
        """Gets the message of this UserResponse.


        :return: The message of this UserResponse.
        :rtype: str
        """
        return self._message

    @message.setter
    def message(self, message: str):
        """Sets the message of this UserResponse.


        :param message: The message of this UserResponse.
        :type message: str
        """
        if message is None:
            raise ValueError("Invalid value for `message`, must not be `None`")  # noqa: E501

        self._message = message
