# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class Assignment(Model):
    """
    Do not edit the class manually.
    """
    def __init__(self, device_id: str=None, user_email: str=None):  # noqa: E501
        """Assignment - a model defined in Swagger

        :param device_id: The device_id of this Assignment.  # noqa: E501
        :type device_id: str
        :param user_email: The user_email of this Assignment.  # noqa: E501
        :type user_email: str
        """
        self.swagger_types = {
            'device_id': str,
            'user_email': str
        }

        self.attribute_map = {
            'device_id': 'Device_id',
            'user_email': 'user_email'
        }
        self._device_id = device_id
        self._user_email = user_email

    @classmethod
    def from_dict(cls, dikt) -> 'Assignment':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The assignment of this Assignment.  # noqa: E501
        :rtype: Assignment
        """
        return util.deserialize_model(dikt, cls)

    @property
    def device_id(self) -> str:
        """Gets the device_id of this Assignment.


        :return: The device_id of this Assignment.
        :rtype: str
        """
        return self._device_id

    @device_id.setter
    def device_id(self, device_id: str):
        """Sets the device_id of this Assignment.


        :param device_id: The device_id of this Assignment.
        :type device_id: str
        """
        if device_id is None:
            raise ValueError("Invalid value for `device_id`, must not be `None`")  # noqa: E501

        self._device_id = device_id

    @property
    def user_email(self) -> str:
        """Gets the user_email of this Assignment.


        :return: The user_email of this Assignment.
        :rtype: str
        """
        return self._user_email

    @user_email.setter
    def user_email(self, user_email: str):
        """Sets the user_email of this Assignment.


        :param user_email: The user_email of this Assignment.
        :type user_email: str
        """
        if user_email is None:
            raise ValueError("Invalid value for `user_email`, must not be `None`")  # noqa: E501

        self._user_email = user_email
