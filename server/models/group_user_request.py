# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class GroupUserRequest(Model):
    """
    Do not edit the class manually.
    """
    def __init__(self, group_id: int=None, user_id: str=None, role_id: int=None):  # noqa: E501
        """GroupUserRequest - a model defined in Swagger

        :param group_id: The group_id of this GroupUserRequest.  # noqa: E501
        :type group_id: int
        :param user_id: The user_id of this GroupUserRequest.  # noqa: E501
        :type user_id: str
        :param role_id: The role_id of this GroupUserRequest.  # noqa: E501
        :type role_id: int
        """
        self.swagger_types = {
            'group_id': int,
            'user_id': str,
            'role_id': int
        }

        self.attribute_map = {
            'group_id': 'group_id',
            'user_id': 'user_id',
            'role_id': 'role_id'
        }
        self._group_id = group_id
        self._user_id = user_id
        self._role_id = role_id

    @classmethod
    def from_dict(cls, dikt) -> 'GroupUserRequest':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The group_user_request of this GroupUserRequest.  # noqa: E501
        :rtype: GroupUserRequest
        """
        return util.deserialize_model(dikt, cls)

    @property
    def group_id(self) -> int:
        """Gets the group_id of this GroupUserRequest.


        :return: The group_id of this GroupUserRequest.
        :rtype: int
        """
        return self._group_id

    @group_id.setter
    def group_id(self, group_id: int):
        """Sets the group_id of this GroupUserRequest.


        :param group_id: The group_id of this GroupUserRequest.
        :type group_id: int
        """
        if group_id is None:
            raise ValueError("Invalid value for `group_id`, must not be `None`")  # noqa: E501

        self._group_id = group_id

    @property
    def user_id(self) -> str:
        """Gets the user_id of this GroupUserRequest.


        :return: The user_id of this GroupUserRequest.
        :rtype: str
        """
        return self._user_id

    @user_id.setter
    def user_id(self, user_id: str):
        """Sets the user_id of this GroupUserRequest.


        :param user_id: The user_id of this GroupUserRequest.
        :type user_id: str
        """
        if user_id is None:
            raise ValueError("Invalid value for `user_id`, must not be `None`")  # noqa: E501

        self._user_id = user_id

    @property
    def role_id(self) -> int:
        """Gets the role_id of this GroupUserRequest.


        :return: The role_id of this GroupUserRequest.
        :rtype: int
        """
        return self._role_id

    @role_id.setter
    def role_id(self, role_id: int):
        """Sets the role_id of this GroupUserRequest.


        :param role_id: The role_id of this GroupUserRequest.
        :type role_id: int
        """
        if role_id is None:
            raise ValueError("Invalid value for `role_id`, must not be `None`")  # noqa: E501

        self._role_id = role_id
