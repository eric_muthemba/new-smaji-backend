# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class AccountRequest(Model):
    """
    Do not edit the class manually.
    """
    def __init__(self, name: str=None, description: str=None, parent_id: int=None):  # noqa: E501
        """AccountRequest - a model defined in Swagger

        :param name: The name of this AccountRequest.  # noqa: E501
        :type name: str
        :param description: The description of this AccountRequest.  # noqa: E501
        :type description: str
        :param parent_id: The parent_id of this AccountRequest.  # noqa: E501
        :type parent_id: int
        """
        self.swagger_types = {
            'name': str,
            'description': str,
            'parent_id': int
        }

        self.attribute_map = {
            'name': 'name',
            'description': 'description',
            'parent_id': 'parent_id'
        }
        self._name = name
        self._description = description
        self._parent_id = parent_id

    @classmethod
    def from_dict(cls, dikt) -> 'AccountRequest':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The account_request of this AccountRequest.  # noqa: E501
        :rtype: AccountRequest
        """
        return util.deserialize_model(dikt, cls)

    @property
    def name(self) -> str:
        """Gets the name of this AccountRequest.


        :return: The name of this AccountRequest.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name: str):
        """Sets the name of this AccountRequest.


        :param name: The name of this AccountRequest.
        :type name: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    @property
    def description(self) -> str:
        """Gets the description of this AccountRequest.


        :return: The description of this AccountRequest.
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description: str):
        """Sets the description of this AccountRequest.


        :param description: The description of this AccountRequest.
        :type description: str
        """

        self._description = description

    @property
    def parent_id(self) -> int:
        """Gets the parent_id of this AccountRequest.


        :return: The parent_id of this AccountRequest.
        :rtype: int
        """
        return self._parent_id

    @parent_id.setter
    def parent_id(self, parent_id: int):
        """Sets the parent_id of this AccountRequest.


        :param parent_id: The parent_id of this AccountRequest.
        :type parent_id: int
        """

        self._parent_id = parent_id
