# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class AccountUserSuspendRequest(Model):
    """
    Do not edit the class manually.
    """
    def __init__(self, account_id: int=None, user_id: str=None, role_id: int=None, suspend: bool=None):  # noqa: E501
        """AccountUserSuspendRequest - a model defined in Swagger

        :param account_id: The account_id of this AccountUserSuspendRequest.  # noqa: E501
        :type account_id: int
        :param user_id: The user_id of this AccountUserSuspendRequest.  # noqa: E501
        :type user_id: str
        :param role_id: The role_id of this AccountUserSuspendRequest.  # noqa: E501
        :type role_id: int
        :param suspend: The suspend of this AccountUserSuspendRequest.  # noqa: E501
        :type suspend: bool
        """
        self.swagger_types = {
            'account_id': int,
            'user_id': str,
            'role_id': int,
            'suspend': bool
        }

        self.attribute_map = {
            'account_id': 'account_id',
            'user_id': 'user_id',
            'role_id': 'role_id',
            'suspend': 'suspend'
        }
        self._account_id = account_id
        self._user_id = user_id
        self._role_id = role_id
        self._suspend = suspend

    @classmethod
    def from_dict(cls, dikt) -> 'AccountUserSuspendRequest':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The account_user_suspend_request of this AccountUserSuspendRequest.  # noqa: E501
        :rtype: AccountUserSuspendRequest
        """
        return util.deserialize_model(dikt, cls)

    @property
    def account_id(self) -> int:
        """Gets the account_id of this AccountUserSuspendRequest.


        :return: The account_id of this AccountUserSuspendRequest.
        :rtype: int
        """
        return self._account_id

    @account_id.setter
    def account_id(self, account_id: int):
        """Sets the account_id of this AccountUserSuspendRequest.


        :param account_id: The account_id of this AccountUserSuspendRequest.
        :type account_id: int
        """
        if account_id is None:
            raise ValueError("Invalid value for `account_id`, must not be `None`")  # noqa: E501

        self._account_id = account_id

    @property
    def user_id(self) -> str:
        """Gets the user_id of this AccountUserSuspendRequest.


        :return: The user_id of this AccountUserSuspendRequest.
        :rtype: str
        """
        return self._user_id

    @user_id.setter
    def user_id(self, user_id: str):
        """Sets the user_id of this AccountUserSuspendRequest.


        :param user_id: The user_id of this AccountUserSuspendRequest.
        :type user_id: str
        """
        if user_id is None:
            raise ValueError("Invalid value for `user_id`, must not be `None`")  # noqa: E501

        self._user_id = user_id

    @property
    def role_id(self) -> int:
        """Gets the role_id of this AccountUserSuspendRequest.


        :return: The role_id of this AccountUserSuspendRequest.
        :rtype: int
        """
        return self._role_id

    @role_id.setter
    def role_id(self, role_id: int):
        """Sets the role_id of this AccountUserSuspendRequest.


        :param role_id: The role_id of this AccountUserSuspendRequest.
        :type role_id: int
        """
        if role_id is None:
            raise ValueError("Invalid value for `role_id`, must not be `None`")  # noqa: E501

        self._role_id = role_id

    @property
    def suspend(self) -> bool:
        """Gets the suspend of this AccountUserSuspendRequest.


        :return: The suspend of this AccountUserSuspendRequest.
        :rtype: bool
        """
        return self._suspend

    @suspend.setter
    def suspend(self, suspend: bool):
        """Sets the suspend of this AccountUserSuspendRequest.


        :param suspend: The suspend of this AccountUserSuspendRequest.
        :type suspend: bool
        """
        if suspend is None:
            raise ValueError("Invalid value for `suspend`, must not be `None`")  # noqa: E501

        self._suspend = suspend
