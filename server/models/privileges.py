# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from server.models.base_model_ import Model
from server import util


class Privileges(Model):
    """

    Do not edit the class manually.
    """
    def __init__(self, email: str=None):  # noqa: E501
        """Privileges - a model defined in Swagger

        :param email: The email of this Privileges.  # noqa: E501
        :type email: str
        """
        self.swagger_types = {
            'email': str
        }

        self.attribute_map = {
            'email': 'email'
        }
        self._email = email

    @classmethod
    def from_dict(cls, dikt) -> 'Privileges':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The privileges of this Privileges.  # noqa: E501
        :rtype: Privileges
        """
        return util.deserialize_model(dikt, cls)

    @property
    def email(self) -> str:
        """Gets the email of this Privileges.


        :return: The email of this Privileges.
        :rtype: str
        """
        return self._email

    @email.setter
    def email(self, email: str):
        """Sets the email of this Privileges.


        :param email: The email of this Privileges.
        :type email: str
        """

        self._email = email
