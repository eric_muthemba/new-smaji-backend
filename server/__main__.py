#!/usr/bin/env python3

import connexion
from server import encoder
from server.utilities import redis, postgres


def main():
    app = connexion.App(__name__, specification_dir='./swagger/')
    app.app.json_encoder = encoder.JSONEncoder
    redis.init()
    postgres.init('postgres://yotdamqlrtfhca:227e367192da10fc3eaf71ed2d34d225119c3241b6ddeeba541db0f38fd74bea@ec2-23-23-184-76.compute-1.amazonaws.com:5432/ddpb0e6tnk271k')
    app.add_api('swagger.yaml', arguments={'title': 'Smaji Data service Api&#x27;s'}, pythonic_params=True)
    app.run(port=8080,debug=True)

if __name__ == '__main__':
    main()
