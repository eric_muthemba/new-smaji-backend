import connexion
import six

from server.models.login_request import LoginRequest  # noqa: E501
from server.models.login_response import LoginResponse  # noqa: E501
from server.models.privileges import Privileges  # noqa: E501
from server.models.two_factor_authentication_response import TwoFactorAuthenticationResponse  # noqa: E501
from server.models.user import User  # noqa: E501
from server.models.user_response import UserResponse  # noqa: E501
from server.models.user_verify import UserVerify  # noqa: E501
from server.models.verify_response import VerifyResponse  # noqa: E501
from server import util


def api_user_apis_user_admin_privileges_class_instance_patch(body):  # noqa: E501
    """elevate privileges

     # noqa: E501

    :param body: Created user object
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = Privileges.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def api_user_apis_user_data_class_instance_get(username):  # noqa: E501
    """Get user by user name

     # noqa: E501

    :param username: The name that needs to be fetched. Use user1 for testing.
    :type username: str

    :rtype: User
    """
    return 'do some magic!'


def api_user_apis_user_deletion_class_instance_delete(username):  # noqa: E501
    """Delete user

    This can only be done by the logged in user. # noqa: E501

    :param username: The name that needs to be deleted
    :type username: str

    :rtype: None
    """
    return 'do some magic!'


def api_user_apis_user_login_class_instance_post(body):  # noqa: E501
    """login

    login user. # noqa: E501

    :param body: login user
    :type body: dict | bytes

    :rtype: LoginResponse
    """
    if connexion.request.is_json:
        body = LoginRequest.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def api_user_apis_user_login_class_instance_verify(body):  # noqa: E501
    """login

    login two factor authentication. # noqa: E501

    :param body: login two factor authentication
    :type body: dict | bytes

    :rtype: LoginResponse
    """
    if connexion.request.is_json:
        body = UserVerify.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def api_user_apis_user_logout_class_instance_get():  # noqa: E501
    """Logs out current logged in user session

     # noqa: E501


    :rtype: None
    """
    return 'do some magic!'


def api_user_apis_user_registration_class_instance_post(body):  # noqa: E501
    """Create user

    This can only be done by the logged in user. # noqa: E501

    :param body: Created user object
    :type body: dict | bytes

    :rtype: UserResponse
    """
    if connexion.request.is_json:
        body = User.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def api_user_apis_user_registration_class_instance_verify(body):  # noqa: E501
    """api_user_apis_user_registration_class_instance_verify

    Verify user phone number by providing the code sent to the phone. # noqa: E501

    :param body: Verify user phone number
    :type body: dict | bytes

    :rtype: VerifyResponse
    """
    if connexion.request.is_json:
        body = UserVerify.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def api_user_apis_user_update_class_instance_put(body, username):  # noqa: E501
    """Updated user

    This can only be done by the logged in user. # noqa: E501

    :param body: Updated user object
    :type body: dict | bytes
    :param username: name that need to be updated
    :type username: str

    :rtype: None
    """
    if connexion.request.is_json:
        body = User.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'
