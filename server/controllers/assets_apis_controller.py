import connexion
import six

from server.models.privileges import Privileges  # noqa: E501
from server.models.tank_object import TankObject  # noqa: E501
from server import util


def assets_apis_class_instance_asset_deletion(asset_id):  # noqa: E501
    """Delete specific asset

    This can only be done by the logged in user. # noqa: E501

    :param asset_id: The Asset that needs to be deleted
    :type asset_id: str

    :rtype: None
    """
    return 'do some magic!'


def assets_apis_class_instance_asset_report(asset_type, body=None):  # noqa: E501
    """Report specific asset.

     # noqa: E501

    :param asset_type: The name of the asset
    :type asset_type: str
    :param body:
    :type body: dict | bytes

    :rtype: None
    """
    print(asset_type)
    if connexion.request.is_json:
        body = TankObject.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def assets_apis_class_instance_asset_report_image(asset_id, asset_image=None):  # noqa: E501
    """uploads asset image

     # noqa: E501

    :param asset_id: The Asset that owns the image
    :type asset_id: str
    :param asset_image:
    :type asset_image: strstr

    :rtype: None
    """
    return 'do some magic!'


def assets_apis_class_instance_update_asset(body, asset_id):  # noqa: E501
    """updates specific asset

     # noqa: E501

    :param body: Created user object
    :type body: dict | bytes
    :param asset_id: The name that needs to be deleted
    :type asset_id: str

    :rtype: None
    """
    if connexion.request.is_json:
        body = Privileges.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'
