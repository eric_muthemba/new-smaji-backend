import connexion
import six

from server.models.push import Push  # noqa: E501
from server import util


def api_data_apis_push_data_class_instance_post(body=None):  # noqa: E501
    """api_data_apis_push_data_class_instance_post

    pushes data to to database # noqa: E501

    :param body: data to add
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = Push.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'
