from sqlalchemy import exc, or_
import time

from server.utilities.user import CurrentUser
from server.utilities.db_model import Groups, Users, UserRoles, Roles
from  server.utilities import  postgres
from server.utilities.generator import GenerateID

class Group:
    """A class used to represent a group

    Attributes
    ----------
    group_id : int, optional
        The id of the group

    Methods
    -------
    new(data,creator)
        Create a new group
    get()
        get the group by id from the database
    getAll(offset, limit)
        Get all groups in the system
    hasPermissions(user_id)
        Check the user permissions to access a group
    canExecute()
        check the group status and validity to execute an action in the system
    addUser()
        Add user to a group
    getUserRole()
        Get group user
    getUsers(offset, limit)
        Get list of group user
    deleteUsers()
        Delete group user
    activateUser()
        Updates the active status of a group user
    suspendUser()
        Update the suspend status of a group user
    """

    def __init__(self, group_id=None):
        """
        Parameters
        ----------
        group_id : int, optional
            The id of the group
        """

        self.id = group_id

        if group_id != None:
            self.query, err = self.get()
            if err != None:
                print('initialize group error:', err)
        else:
            self.query = None

    def new(self,data,creator):
        """Create a new group

        Parameters
        ----------
        data : dict
            Group details as submitted by the user

        Return
        ------
        groupID: int
            ID of the new group or None
        error: str
            Error that occurred or None
        """

        groupID = GenerateID()
        
        try:

            # Create group
            postgres.session.add(Groups(id=groupID, g_account_id=data['account_id'], g_parent_id=data['parent_id'] or None, g_name=data['name'], g_description=data['description'] or None, 
                g_created_by=creator, created_on=int(time.time()), modified_on=int(time.time())))

            # Create user role
            postgres.session.add(UserRoles(user_id=creator, role_id=7, account_id=0, group_id=groupID, created_on=int(time.time()), modified_on=int(time.time())))

            postgres.session.commit()
        except exc.SQLAlchemyError as e:

            postgres.session.rollback()

            print('error: ', e)

            return None, 'error saving group details'
        else:
            print('group ', data['name'], ' (ID: ', groupID, ')', ' has been created successfully')
            return groupID, None
        finally:
            postgres.session.close()
    
    def get(self):
        """Get the group by id from the database

        Return
        ------
        query: object
            query object of the data found or None
        error: str
            Error that has occurred or None
        """

        if self.id == None:
            return None, 'group id is required'
        
        try:

            group_query = postgres.session.query(Groups).filter(Groups.id==self.id, Groups.g_is_deleted==False).first()

        except exc.SQLAlchemyError as e:
            print('error getting group:', e)
            return None, 'an error occurred getting group'
        else:
            if group_query:
                return group_query, None

            return None, 'group not found'
        finally:
            postgres.session.close()

    def getAll(self, offset, limit):
        """Get all group in the system

        Parameters
        ----------
        offset : int
            The number of items to skip before starting to collect the result set
        limit : int
            The numbers of items to return

        Return
        ------
        query: object
            The users query (Groups, join UserRoles) or None
        error: str
            Error that has occurred or None
        """
        
        try:

            groups_query = postgres.session.query(Groups, UserRoles).filter(or_(UserRoles.role_id==1, UserRoles.role_id==2, UserRoles.role_id==3), Groups.g_is_deleted==False).offset(offset).limit(limit).all()

        except exc.SQLAlchemyError as e:
            print('get group list error: ', e)
            return None, 'error getting groups list'
        else:

            if groups_query:
                return groups_query, None

            return None, 'groups not found or you do not have the required permissions'
        finally:
            postgres.session.close()

    def hasPermissions(self, user_id):
        """Check the user permissions to access a group

        Parameters
        ----------
        user_id : str
            The id of the user that needs their group access checked

        Return
        ------
        str
            message to inform the user why permission is denied or None
        """

        if self.id == None:
            return None
        
        try:
            role_query = postgres.session.query(Groups, UserRoles).filter(or_(Groups.id == UserRoles.group_id, UserRoles.role_id == 1, UserRoles.role_id == 2, UserRoles.role_id == 3), Groups.id == self.id, Groups.g_is_deleted == False, UserRoles.user_id == user_id, UserRoles.ur_is_deleted == False).first()
        except exc.SQLAlchemyError as e:
            print('error getting user group permissions:', e)
            return 'error getting user group permissions'
        else:
            if role_query:
                if role_query.UserRoles.ur_is_active == False:
                    return 'your group access permission is not activated'

                if role_query.UserRoles.ur_is_suspended == True:
                    return 'your group access permission is suspended'

                return None
            else:
                return 'you have no permission to access this group'
        finally:
            postgres.session.close()

    def canExecute(self):
        """Check the group status and validity to execute an action in the system

        Return
        ------
        str
            message describing the failed status check or None

        """

        if self.query == None:
            return 'group not found'

        if self.query.g_is_active == False:
            return 'group is not active'

        if self.query.g_is_suspended:
            return 'group is suspended'

        return None

    def addUser(self, user_id, role_id):
        """Add user to a group

        Return
        ------
        error : str
            Error that occurred or None

        """

        if user_id == None or user_id == '':
            return 'user id is required'

        if role_id == None or role_id <= 0:
            return 'valid role id is required'

        if role_id == 1 or role_id == 2 or role_id == 3 or role_id == 4 or role_id == 5 or role_id == 6 or role_id == 10:
            return 'invalid role id'

        # Check if group user already exists or is deleted
        try:

            user_query = postgres.session.query(UserRoles).filter(UserRoles.user_id==user_id, UserRoles.role_id==role_id, UserRoles.group_id==self.id, UserRoles.account_id==0, UserRoles.ur_is_deleted==True).first()

            postgres.session.commit()

        except exc.SQLAlchemyError as e:

            postgres.session.rollback()

            print('error: ', e)

            return 'error getting group users'
        else:

            if user_query:

                if user_query.ur_is_deleted == False:
                        return 'user already exists in the group'

                user_query.ur_is_deleted = False
                user_query.ur_is_deleted_on = None                
                user_query.modified_on = int(time.time())

                try:
                    postgres.session.commit()
                except exc.SQLAlchemyError as e:

                    postgres.session.rollback()

                    print('error: ', e)

                    return 'error updating group user delete status'
                else:
                    print('group user (ID: ' + user_id + ', role: ' + str(role_id) + ') delete status updated')
                    return None               
        finally:
            postgres.session.close()

        # Add group user
        try:

            postgres.session.add(UserRoles(user_id=user_id, role_id=role_id, group_id=self.id, account_id=0, created_on=int(time.time()), modified_on=int(time.time())))

            postgres.session.commit()

        except exc.IntegrityError as e:
            return 'user already exists in the group'
        except exc.SQLAlchemyError as e:

            postgres.session.rollback()

            print('saving new group user error: ', e)

            return 'error saving new group user'
        else:
            return None
        finally:
            postgres.session.close()
    
    def getUserRole(self, user_id, role_id):
        """Get group user

        Return
        ------
        query: object
            The user role query or None
        error: str
            Error that occurred or None
        """

        try:

            role_query = postgres.session.query(UserRoles).filter(UserRoles.user_id==user_id, UserRoles.role_id==role_id, UserRoles.group_id==self.id, UserRoles.account_id==0, UserRoles.ur_is_deleted==False).first()

        except exc.SQLAlchemyError as e:

            print('error: ', e)

            return None, 'error getting group user role'
        else:

            if role_query:
                return role_query, None
            else:
                return None, 'user role not found'

        finally:
            postgres.session.close()

    def getUsers(self, offset, limit):
        """get list of user role

        Parameters
        ----------
        offset : int
            The number of items to skip before starting to collect the result set
        limit : int
            The numbers of items to return

        Return
        ------
        query: object
            The users query (UserRoles, join Users) or None
        error: str
            Error that has occurred or None
        """

        if offset == None or offset <= 0:
            offset = 0

        if limit == None or limit <= 0:
            limit = offset + 1000

        try:

            users_query = postgres.session.query(UserRoles, Users, Roles).filter(UserRoles.user_id == Users.id, UserRoles.role_id == Roles.id, UserRoles.group_id == self.id, UserRoles.ur_is_deleted == False, Users.is_deleted == False).offset(offset).limit(limit).all()

        except exc.SQLAlchemyError as e:

            print('get group users error: ', e)

            return None, 'error getting group users'
        else:

            if users_query:
                return users_query, None
            else:
                return None, 'user role does not exist'
                
        finally:
            postgres.session.close()

    def deleteUsers(self, user_id, role_id, is_deleted):
        """Delete group user

        Return
        ------
        error: str
            Error or None

        """

        try:

            user_query = postgres.session.query(UserRoles).filter(UserRoles.user_id==user_id, UserRoles.role_id==role_id, UserRoles.group_id==self.id, UserRoles.account_id==0, UserRoles.ur_is_deleted==False).first()

            if user_query.ur_is_deleted:
                    return 'user role does not exist'

            user_query.ur_is_deleted = is_deleted
            user_query.modified_on = int(time.time())

            if is_deleted:
                user_query.ur_is_deleted_on = user_query.modified_on
            else:
                user_query.ur_is_deleted_on = None

            postgres.session.commit()

        except exc.SQLAlchemyError as e:

            postgres.session.rollback()

            print('get and update group user error: ', e)

            return 'error getting and updating group user'
        else:
            return None                
        finally:
            postgres.session.close()

    def activateUser(self, user_id, role_id, is_active):
        """Updates the active status of a group user

        Return
        ------
        error: str
            Error or None

        """

        try:

            user_query = postgres.session.query(UserRoles).filter(UserRoles.user_id==user_id, UserRoles.role_id==role_id, UserRoles.group_id==self.id, UserRoles.account_id==0, UserRoles.ur_is_deleted==False).first()

            if user_query.ur_is_deleted:
                    return 'user role does not exist'

            user_query.ur_is_active = is_active

            if is_active:
                user_query.ur_is_deactivated_on = None
            else:
                user_query.ur_is_deactivated_on = int(time.time())

            user_query.modified_on = int(time.time())

            postgres.session.commit()

        except exc.SQLAlchemyError as e:

            postgres.session.rollback()

            print('group update activate error: ', e)

            return 'error trying to update a group user active status'
        else:
            return None                
        finally:
            postgres.session.close()

    def suspendUser(self, user_id, role_id, is_suspended):
        """Update the suspend status of a group user

        Return
        ------
        error: str
            Error or None

        """

        try:

            user_query = postgres.session.query(UserRoles).filter(UserRoles.user_id==user_id, UserRoles.role_id==role_id, UserRoles.group_id==self.id, UserRoles.account_id==0, UserRoles.ur_is_deleted==False).first()
            
            if user_query.ur_is_deleted:
                    return 'user role does not exist'

            user_query.ur_is_suspended = is_suspended
            user_query.modified_on = int(time.time())

            postgres.session.commit()

        except exc.SQLAlchemyError as e:

            postgres.session.rollback()

            print('update group suspend status error: ', e)

            return 'error trying to update a group user suspend status'
        else:
            return None                
        finally:
            postgres.session.close()