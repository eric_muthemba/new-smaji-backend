import connexion
import six

from server.models.group_request import GroupRequest  # noqa: E501
from server.models.group_user_activate_request import GroupUserActivateRequest  # noqa: E501
from server.models.group_user_delete_request import GroupUserDeleteRequest  # noqa: E501
from server.models.group_user_request import GroupUserRequest  # noqa: E501
from server.models.group_user_suspend_request import GroupUserSuspendRequest  # noqa: E501
from server import util

from server.utilities.user import CurrentUserCanExecute, CurrentUser, User
from server.controllers.Grouping_controller.group import Group
from server.controllers.account_controller.account import Account


def CurrentUserID(self):
    """A valid user currently making the request"""
    return CurrentUser()['id']

def api_data_apis_group_class_instance_activate_user(body=None):  # noqa: E501
    """api_data_apis_group_class_instance_activate_user

    update the group user active status. # noqa: E501

    :param body:
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = GroupUserActivateRequest.from_dict(connexion.request.get_json())  # noqa: E501
        err = CurrentUserCanExecute()
        if err != None:
            return {
                       'success': False,
                       'message': err
                   }, 403

        group = Group(body.group_id)

        # Check if group is cleared to execute
        executeError = group.canExecute()

        if executeError != None:
            return {
                       'success': False,
                       'message': executeError
                   }, 403

        # Check if user has permissions to access group
        permissionError = group.hasPermissions(CurrentUserID())

        if permissionError != None:
            return {
                       'success': False,
                       'message': permissionError
                   }, 403

        err = group.activateUser(body.user_id, body.role_id, body.active)

        if err != None:
            return {
                       'success': False,
                       'message': err
                   }, 500

        return {
                   'success': True,
                   'message ': 'group user active status updated successfully'
               }, 200

def api_data_apis_group_class_instance_add_user(body=None):  # noqa: E501
    """api_data_apis_group_class_instance_add_user

    Add user to an group. # noqa: E501

    :param body:
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = GroupUserRequest.from_dict(connexion.request.get_json())  # noqa: E501
        err = CurrentUserCanExecute()
        if err != None:
            return {
                       'success': False,
                       'message': err
                   }, 403

        group = Group(body.group_id)

        # Check if group is cleared to execute
        executeError = group.canExecute()

        if executeError != None:
            return {
                       'success': False,
                       'message': executeError
                   }, 403

        # Check if user has permissions to access group
        permissionError = group.hasPermissions(CurrentUserID())

        if permissionError != None:
            return {
                       'success': False,
                       'message': permissionError
                   }, 403

        # Check if user being added to group is cleared to execute
        user = User(body.user_id)

        userExecuteError = user.canExecute()

        if userExecuteError != None:
            return {
                       'success': False,
                       'message': 'user (ID:' + body.user_id + '): ' + userExecuteError
                   }, 403

        # Add user
        err = group.addUser(body.user_id, body.role_id)

        if err != None:
            return {
                       'success': False,
                       'message': err
                   }, 500

        return {
                   'success': True,
                   'message ': 'user added to group successfully'
               }, 201


def api_data_apis_group_class_instance_delete_user(body=None):  # noqa: E501
    """api_data_apis_group_class_instance_delete_user

    update the group user delete status. # noqa: E501

    :param body:
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = GroupUserDeleteRequest.from_dict(connexion.request.get_json())  # noqa: E501
        err = CurrentUserCanExecute()
        if err != None:
            return {
                       'success': False,
                       'message': err
                   }, 403

        group = Group(body.group_id)

        # Check if group is cleared to execute
        executeError = group.canExecute()

        if executeError != None:
            return {
                       'success': False,
                       'message': executeError
                   }, 403

        # Check if user has permissions to access group
        permissionError = group.hasPermissions(CurrentUserID())

        if permissionError != None:
            return {
                       'success': False,
                       'message': permissionError
                   }, 403

        err = group.deleteUsers(body.user_id, body.role_id, True)

        if err != None:
            return {
                       'success': False,
                       'message': err
                   }, 500

        return {
                   'success': True,
                   'message ': 'group user deleted successfully'
               }, 200


def api_data_apis_group_class_instance_get(group_id):  # noqa: E501
    """api_data_apis_group_class_instance_get

    gets group by id # noqa: E501

    :param group_id: The id of a group
    :type group_id: int

    :rtype: None
    """
    err = CurrentUserCanExecute()
    if err != None:
        return {
                   'success': False,
                   'message': err
               }, 403

    group = Group(group_id)

    # Check if group is cleared to execute
    executeError = group.canExecute()

    if executeError != None:
        return {
                   'success': False,
                   'message': executeError
               }, 403

    # Check if user has permissions to access group
    permissionError = group.hasPermissions(CurrentUserID())

    if permissionError != None:
        return {
                   'success': False,
                   'message': permissionError
               }, 403

    return {
               "id": group.query.id,
               "name": group.query.g_name,
               "description": group.query.g_description,
               "device_count": group.query.g_device_count,
               "tank_count": group.query.g_tank_count,
               "is_suspended": group.query.g_is_suspended,
               "is_active": group.query.g_is_active
           }, 200


def api_data_apis_group_class_instance_get_all(offset=None, limit=None):  # noqa: E501
    """api_data_apis_group_class_instance_get_all

    gets groups list # noqa: E501

    :param offset: The number of items to skip before starting to collect the result set
    :type offset: int
    :param limit: The numbers of items to return
    :type limit: int

    :rtype: None
    """
    err = CurrentUserCanExecute()
    if err != None:
        return {
                   'success': False,
                   'message': err
               }, 403

    groups_query, err = Group().getAll(offset, limit)

    if err != None:
        return {
                   'success': False,
                   'message': err
               }, 500

    data = []

    for g in groups_query:
        data.append({
            "id": g.Groups.id,
            "name": g.Groups.g_name,
            "device_count": g.Groups.g_device_count,
            "tank_count": g.Groups.g_tank_count,
            "is_suspended": g.Groups.g_is_suspended,
            "is_active": g.Groups.g_is_active
        })

    return {
               'success': True,
               "count": len(data),
               "offset": offset,
               "limit": limit,
               "groups": data
           }, 200


def api_data_apis_group_class_instance_get_users(group_id, offset=None, limit=None):  # noqa: E501
    """api_data_apis_group_class_instance_get_users

    gets group by id # noqa: E501

    :param group_id: The id of a group
    :type group_id: int
    :param offset: The number of items to skip before starting to collect the result set
    :type offset: int
    :param limit: The numbers of items to return
    :type limit: int

    :rtype: None
    """
    err = CurrentUserCanExecute()
    if err != None:
        return {
                   'success': False,
                   'message': err
               }, 403

    # Get the group
    group = Group(group_id)

    # Check if group is cleared to execute
    executeError = group.canExecute()

    if executeError != None:
        return {
                   'success': False,
                   'message': executeError
               }, 403

    # Check if user has permissions to access group
    permissionError = group.hasPermissions(CurrentUserID())

    if permissionError != None:
        return {
                   'success': False,
                   'message': permissionError
               }, 403

    # Get group users
    group_users_query, err = group.getUsers(offset, limit)

    if err != None:
        return {
                   'success': False,
                   'message': err
               }, 500

    data = []

    for user in group_users_query:
        data.append({
            "user_id": user.UserRoles.user_id,
            "first_name": user.Users.u_first_name,
            "last_name": user.Users.u_last_name,
            "phone_area": user.Users.u_phone_area,
            "phone_number": user.Users.u_phone_number,
            "role_id": user.UserRoles.role_id,
            "role_name": user.Roles.r_name,
            "is_suspended": user.UserRoles.ur_is_suspended,
            "is_active": user.UserRoles.ur_is_active
        })

    return {
               "success": True,
               "count": len(data),
               "offset": offset,
               "limit": limit,
               "users": data
           }, 200


def api_data_apis_group_class_instance_post(body=None):  # noqa: E501
    """api_data_apis_group_class_instance_post

    Create a new group. # noqa: E501

    :param body:
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = GroupRequest.from_dict(connexion.request.get_json())  # noqa: E501
        err = CurrentUserCanExecute()
        if err != None:
            return {
                       'success': False,
                       'message': err
                   }, 403

        account = Account(body.account_id)

        # Check if account is cleared to execute
        executeError = account.canExecute()

        if executeError != None:
            return {
                       'success': False,
                       'message': executeError
                   }, 403

        # Check if user has permissions to access account
        permissionError = account.hasPermissions(CurrentUserID())

        if permissionError != None:
            return {
                       'success': False,
                       'message': permissionError
                   }, 403

        if body.parent_id > 0:

            parentGroup = Group(body.parent_id)

            # Check if parent group is cleared to execute
            parentExecuteError = parentGroup.canExecute()

            if parentExecuteError != None:
                return {
                           'success': False,
                           'message': parentExecuteError
                       }, 403

            # Check if user has permissions to access parent group
            permissionError = parentGroup.hasPermissions(CurrentUserID())

            if permissionError != None:
                return {
                           'success': False,
                           'message': permissionError
                       }, 403

        groupID, err = Group().new(Data, CurrentUserID())

        if err != None:
            return {
                       'success': False,
                       'message': err
                   }, 500

        return {
                   'success': True,
                   'group_id': groupID,
                   'message ': 'new group created successfully'
               }, 201


def api_data_apis_group_class_instance_suspend_user(body=None):  # noqa: E501
    """api_data_apis_group_class_instance_suspend_user

    update the group user suspend status. # noqa: E501

    :param body: 
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = GroupUserSuspendRequest.from_dict(connexion.request.get_json())  # noqa: E501
        err = CurrentUserCanExecute()
        if err != None:
            return {
                       'success': False,
                       'message': err
                   }, 403

        group = Group(body.group_id)

        # Check if group is cleared to execute
        executeError = group.canExecute()

        if executeError != None:
            return {
                       'success': False,
                       'message': executeError
                   }, 403

        # Check if user has permissions to access group
        permissionError = group.hasPermissions(CurrentUserID())

        if permissionError != None:
            return {
                       'success': False,
                       'message': permissionError
                   }, 403

        err = group.suspendUser(body.user_id, body.role_id, body.suspend)

        if err != None:
            return {
                       'success': False,
                       'message': err
                   }, 500

        return {
                   'success': True,
                   'message ': 'group user suspend status updated successfully'
               }, 200