from sqlalchemy import exc, or_
import time

from server.utilities.user import CurrentUser
from server.utilities.db_model import Accounts, Users, UserRoles, Roles
from server.utilities import postgres
from server.utilities.generator import GenerateID

class Account:
    """A class used to represent an account

    Attributes
    ----------
    account_id : int, optional
        The id of the account

    Methods
    -------
    new(data,creator)
        Create a new account
    get()
        get the account by id from the database
    getAll(offset, limit)
        Get all account in the system
    hasPermissions(user_id)
        Check the user permissions to access an account
    canExecute()
        check the account status and validity to execute an action in the system
    addUser()
        Add user to an account
    getUserRole()
        Get account user
    getUsers(offset, limit)
        Get list of account user
    deleteUsers()
        Delete account user
    activateUser()
        Updates the active status of an account user
    suspendUser()
        Update the suspend status of an account user
    """

    def __init__(self, account_id=None):
        """
        Parameters
        ----------
        account_id : int, optional
            The id of the account
        """

        self.id = account_id

        if account_id != None:
            self.query, err = self.get()
            if err != None:
                print('initialize account error:', err)
        else:
            self.query = None

    def new(self,data,creator):
        """Create a new account

        Parameters
        ----------
        data : dict
            Account details as submitted by the user

        Return
        ------
        accountID: int
            ID of the new account or None
        error: str
            Error that occurred or None
        """

        accountID = GenerateID()
        
        try:

            # Create account
            postgres.session.add(Accounts(id=accountID, a_parent_id=data['parent_id'] or None, a_name=data['name'], a_description=data['description'] or None, created_by=creator, created_on=int(time.time()), modified_on=int(time.time())))
            
            # Create user role
            postgres.session.add(UserRoles(user_id=creator, role_id=5, account_id=accountID, group_id=0, created_on=int(time.time()), modified_on=int(time.time())))

            postgres.session.commit()
        except exc.SQLAlchemyError as e:

            postgres.session.rollback()

            print('error: ', e)

            return None, 'error saving account details'
        else:
            print('account ', data['name'], ' (ID: ', accountID, ')', ' has been created successfully')
            return accountID, None
        finally:
            postgres.session.close()

    def get(self):
        """Get the account by id from the database

        Return
        ------
        query: object
            query object of the data found or None
        error: str
            Error that has occurred or None
        """

        if self.id == None:
            return None, 'account id is required'
        
        try:
            account_query = postgres.session.query(Accounts).filter(Accounts.id == self.id, Accounts.is_deleted == False).first()
        except exc.SQLAlchemyError as e:
            print('error getting account:', e)
            return None, 'an error occurred getting account'
        else:
            if account_query:
                return account_query, None
            
            return None, 'account not found'    
        finally:
            postgres.session.close()

    def getAll(self, offset, limit):
        """Get all account in the system

        Parameters
        ----------
        offset : int
            The number of items to skip before starting to collect the result set
        limit : int
            The numbers of items to return

        Return
        ------
        query: object
            The users query (Accounts, join UserRoles) or None
        error: str
            Error that has occurred or None
        """

        if offset == None or offset <= 0:
            offset = 0

        if limit == None or limit <= 0:
            limit = offset + 1000

        try:

            accounts_query = postgres.session.query(Accounts, UserRoles).filter(or_(UserRoles.role_id==1, UserRoles.role_id==2, UserRoles.role_id==3), Accounts.is_deleted==False).offset(offset).limit(limit).all()

        except exc.SQLAlchemyError as e:

            print('error: ', e)

            return None, 'error getting accounts list'
        else:
            if accounts_query:
                return accounts_query, None
            
            return None, 'accounts not found or you do not have the required permissions'            
        finally:
            postgres.session.close()

    def hasPermissions(self, user_id):
        """Check the user permissions to access an account

        Parameters
        ----------
        user_id : str
            The id of the user that needs their account access checked

        Return
        ------
        str
            message to inform the user why permission is denied or None
        """

        if self.id == None:
            return None
        
        try:
            role_query = postgres.session.query(Accounts, UserRoles).filter(or_(Accounts.id == UserRoles.account_id, UserRoles.role_id == 1, UserRoles.role_id == 2, UserRoles.role_id == 3), Accounts.id == self.id, Accounts.is_deleted == False, UserRoles.user_id == user_id, UserRoles.ur_is_deleted == False).first()
        except exc.SQLAlchemyError as e:
            print('error getting user account permissions:', e)
            return 'error getting user account permissions'
        else:
            if role_query:
                if role_query.UserRoles.ur_is_active == False:
                    return 'your account access permission is not activated'

                if role_query.UserRoles.ur_is_suspended == True:
                    return 'your account access permission is suspended'

                return None
            else:
                return 'you have no permission to access this account'
        finally:
            postgres.session.close()

    def canExecute(self):
        """Check the account status and validity to execute an action in the system

        Return
        ------
        str
            message describing the failed status check or None

        """

        if self.query == None:
            return 'account not found'

        if self.query.is_active == False:
            return 'account is not active'

        if self.query.is_suspended:
            return 'account is suspended'

        return None

    def addUser(self, user_id, role_id):
        """Add user to an account

        Return
        ------
        error : str
            Error that occurred or None

        """

        if user_id == None or user_id == '':
            return 'user id is required'

        if role_id == None or role_id <= 0:
            return 'valid role id is required'

        if role_id == 1 or role_id == 2 or role_id == 3 or role_id == 4 or role_id == 7 or role_id == 8 or role_id == 9 or role_id == 10:
            return 'invalid role id'

        # Check if account user already exists or is deleted
        try:

            user_query = postgres.session.query(UserRoles).filter(UserRoles.user_id==user_id, UserRoles.role_id==role_id, UserRoles.account_id==self.id, UserRoles.group_id==0, UserRoles.ur_is_deleted==True).first()

            postgres.session.commit()

        except exc.SQLAlchemyError as e:

            postgres.session.rollback()

            print('error: ', e)

            return 'error getting account users'
        else:

            if user_query:

                if user_query.ur_is_deleted == False:
                        return 'user already exists in the account'

                user_query.ur_is_deleted = False
                user_query.ur_is_deleted_on = None                
                user_query.modified_on = int(time.time())

                try:
                    postgres.session.commit()
                except exc.SQLAlchemyError as e:

                    postgres.session.rollback()

                    print('error: ', e)

                    return 'error updating account user delete status'
                else:
                    print('account user (ID: ' + user_id +', role: ' + str(role_id) + ') delete status updated')
                    return None               
        finally:
            postgres.session.close()

        # Add account user
        try:

            postgres.session.add(UserRoles(user_id=user_id, role_id=role_id, account_id=self.id, group_id=0, created_on=int(time.time()), modified_on=int(time.time())))

            postgres.session.commit()

        except exc.IntegrityError as e:
            return 'user already exists in the account'
        except exc.SQLAlchemyError as e:

            postgres.session.rollback()

            print('error: ', e)

            return 'error saving new account user'
        else:
            return None
        finally:
            postgres.session.close()

    def getUserRole(self, user_id, role_id):
        """Get account user

        Return
        ------
        query: object
            The user role query or None
        error: str
            Error that occurred or None
        """

        try:

            role_query = postgres.session.query(UserRoles).filter(UserRoles.user_id==user_id, UserRoles.role_id==role_id, UserRoles.account_id==self.id, UserRoles.group_id==0, UserRoles.ur_is_deleted==False).first()

        except exc.SQLAlchemyError as e:

            print('error: ', e)

            return None, 'error getting account user role'
        else:

            if role_query:
                return role_query, None
            else:
                return None, 'user role not found'

        finally:
            postgres.session.close()

    def getUsers(self, offset, limit):
        """get list of user role

        Parameters
        ----------
        offset : int
            The number of items to skip before starting to collect the result set
        limit : int
            The numbers of items to return

        Return
        ------
        query: object
            The users query (UserRoles, join Users) or None
        error: str
            Error that has occurred or None
        """

        if offset == None or offset <= 0:
            offset = 0

        if limit == None or limit <= 0:
            limit = offset + 1000

        try:

            users_query = postgres.session.query(UserRoles, Users, Roles).filter(UserRoles.user_id == Users.id, UserRoles.role_id == Roles.id, UserRoles.account_id == self.id, UserRoles.ur_is_deleted == False, Users.is_deleted == False).offset(offset).limit(limit).all()

        except exc.SQLAlchemyError as e:

            print('error: ', e)

            return None, 'error getting account users'
        else:

            if users_query:
                return users_query, None
            else:
                return None, 'user role does not exist'
                
        finally:
            postgres.session.close()

    def deleteUsers(self, user_id, role_id, is_deleted):
        """Delete account user

        Return
        ------
        error: str
            Error or None

        """

        try:

            user_query = postgres.session.query(UserRoles).filter(UserRoles.user_id==user_id, UserRoles.role_id==role_id, UserRoles.account_id==self.id, UserRoles.group_id==0, UserRoles.ur_is_deleted==False).first()

            if user_query.ur_is_deleted:
                    return 'user role does not exist'

            user_query.ur_is_deleted = is_deleted
            user_query.modified_on = int(time.time())

            if is_deleted:
                user_query.ur_is_deleted_on = user_query.modified_on
            else:
                user_query.ur_is_deleted_on = None

            postgres.session.commit()

        except exc.SQLAlchemyError as e:

            postgres.session.rollback()

            print('get and update account user error: ', e)

            return 'error getting and updating account user'
        else:
            return None                
        finally:
            postgres.session.close()

    def activateUser(self, user_id, role_id, is_active):
        """Updates the active status of an account user

        Return
        ------
        error: str
            Error or None

        """

        try:

            user_query = postgres.session.query(UserRoles).filter(UserRoles.user_id==user_id, UserRoles.role_id==role_id, UserRoles.account_id==self.id, UserRoles.group_id==0, UserRoles.ur_is_deleted==False).first()

            if user_query.ur_is_deleted:
                    return 'user role does not exist'

            user_query.ur_is_active = is_active

            if is_active:
                user_query.ur_is_deactivated_on = None
            else:
                user_query.ur_is_deactivated_on = int(time.time())

            user_query.modified_on = int(time.time())

            postgres.session.commit()

        except exc.SQLAlchemyError as e:

            postgres.session.rollback()

            print('error: ', e)

            return 'error trying to update an account user active status'
        else:
            return None                
        finally:
            postgres.session.close()

    def suspendUser(self, user_id, role_id, is_suspended):
        """Update the suspend status of an account user

        Return
        ------
        error: str
            Error or None

        """

        try:

            user_query = postgres.session.query(UserRoles).filter(UserRoles.user_id==user_id, UserRoles.role_id==role_id, UserRoles.account_id==self.id, UserRoles.group_id==0, UserRoles.ur_is_deleted==False).first()
            
            if user_query.ur_is_deleted:
                    return 'user role does not exist'

            user_query.ur_is_suspended = is_suspended
            user_query.modified_on = int(time.time())

            postgres.session.commit()

        except exc.SQLAlchemyError as e:

            postgres.session.rollback()

            print('error: ', e)

            return 'error trying to update an account user suspend status'
        else:
            return None                
        finally:
            postgres.session.close()

