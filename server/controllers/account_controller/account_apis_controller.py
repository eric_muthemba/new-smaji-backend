import connexion

from server.models.account_request import AccountRequest
from server.models.account_user_activate_request import AccountUserActivateRequest
from server.models.account_user_delete_request import AccountUserDeleteRequest
from server.models.account_user_request import AccountUserRequest
from server.models.account_user_suspend_request import AccountUserSuspendRequest

from server.utilities.user import CurrentUserCanExecute, User
from server.controllers.account_controller.account import Account

def activateUser(body=None):
    """activate_user

    update the account user active status. # noqa: E501

    :param body:
    :type body: dict | bytes

    :rtype: str
        Error or None
    """

    if connexion.request.is_json:
        body = AccountUserActivateRequest.from_dict(connexion.request.get_json())
        err = CurrentUserCanExecute()
        if err != None:
            return {
                       'success': False,
                       'message': err
                   }, 403

        account = Account(body.account_id)

        # Check if account is cleared to execute
        executeError = account.canExecute()

        if executeError != None:
            return {
                       'success': False,
                       'message': executeError
                   }, 403

        # Check if user has permissions to access account
        permissionError = account.hasPermissions(self.CurrentUserID())

        if permissionError != None:
            return {
                       'success': False,
                       'message': permissionError
                   }, 403

        err = account.activateUser(body.user_id, body.role_id, body.active)

        if err != None:
            return {
                       'success': False,
                       'message': err
                   }, 500

        return {
                   'success': True,
                   'message ': 'account user active status updated successfully'
               }, 200
            

def add_user(body=None):
    """add_user

    Add user to an account. # noqa: E501

    :param body:
    :type body: dict | bytes

    :rtype: json
    """
    if connexion.request.is_json:
        body = AccountUserRequest.from_dict(connexion.request.get_json())
        err = CurrentUserCanExecute()
        if err != None:
            return {
                       'success': False,
                       'message': err
                   }, 403

        account = Account(body.account_id)

        # Check if account is cleared to execute
        executeError = account.canExecute()

        if executeError != None:
            return {
                       'success': False,
                       'message': executeError
                   }, 403

        # Check if user has permissions to access account
        permissionError = account.hasPermissions(self.CurrentUserID())

        if permissionError != None:
            return {
                       'success': False,
                       'message': permissionError
                   }, 403

            # Check if user being added to account is cleared to execute
        user = User(body.user_id)

        userExecuteError = user.canExecute()

        if userExecuteError != None:
            return {
                       'success': False,
                       'message': 'user (ID:' + body.user_id + '): ' + userExecuteError
                   }, 403

        err = account.addUser(body.user_id, body.role_id)

        if err != None:
            return {
                       'success': False,
                       'message': err
                   }, 500

        return {
                   'success': True,
                   'message ': 'user added to account successfully'
               }, 201


def delete_user(body=None):
    """delete_user

    update the account user delete status. # noqa: E501

    :param body:
    :type body: dict | bytes

    :rtype: json
    """
    if connexion.request.is_json:
        body = AccountUserDeleteRequest.from_dict(connexion.request.get_json())
        err = CurrentUserCanExecute()
        if err != None:
            return {
                       'success': False,
                       'message': err
                   }, 403

        account = Account(body.account_id)

        # Check if account is cleared to execute
        executeError = account.canExecute()

        if executeError != None:
            return {
                       'success': False,
                       'message': executeError
                   }, 403

        # Check if user has permissions to access account
        permissionError = account.hasPermissions(self.CurrentUserID())

        if permissionError != None:
            return {
                       'success': False,
                       'message': permissionError
                   }, 403

        err = account.deleteUsers(body.user_id, body.role_id, True)

        if err != None:
            return {
                       'success': False,
                       'message': err
                   }, 500

        return {
                   'success': True,
                   'message ': 'account user deleted successfully'
               }, 200


def get(account_id):
    """get

    gets account by id # noqa: E501

    :param account_id: The id of a account
    :type account_id: int

    :rtype: json
    """
    err = CurrentUserCanExecute()
    if err != None:
        return {
                   'success': False,
                   'message': err
               }, 403

    account = Account(account_id)

    # Check if account is cleared to execute
    executeError = account.canExecute()

    if executeError != None:
        return {
                   'success': False,
                   'message': executeError
               }, 403

    # Check if user has permissions to access account
    permissionError = account.hasPermissions(self.CurrentUserID())

    if permissionError != None:
        return {
                   'success': False,
                   'message': permissionError
               }, 403

    return {
               "success": True,
               "id": account.query.id,
               "name": account.query.a_name,
               "description": account.query.a_description,
               "is_suspended": account.query.is_suspended,
               "is_active": account.query.is_active
           }, 200


def get_all(offset=None, limit=None):
    """get_all

    gets accounts list # noqa: E501

    :param offset: The number of items to skip before starting to collect the result set
    :type offset: int
    :param limit: The numbers of items to return
    :type limit: int

    :rtype: json
    """
    print(offset)
    err = CurrentUserCanExecute()
    if err != None:
        return {
                   'success': False,
                   'message': err
               }, 403

    accounts_query, err = Account().getAll(offset, limit)

    if err != None:
        return {
                   'success': False,
                   'message': err
               }, 500

    data = []

    for a in accounts_query:
        data.append({
            "id": a.Accounts.id,
            "name": a.Accounts.a_name,
            "is_suspended": a.Accounts.is_suspended,
            "is_active": a.Accounts.is_active
        })

    return {
               'success': True,
               "count": len(data),
               "offset": offset,
               "limit": limit,
               "accounts": data
           }, 200


def get_users(account_id, offset=None, limit=None):
    """get_users

    gets account by id # noqa: E501

    :param account_id: The id of a account
    :type account_id: int
    :param offset: The number of items to skip before starting to collect the result set
    :type offset: int
    :param limit: The numbers of items to return
    :type limit: int

    :rtype: json
    """
    err = CurrentUserCanExecute()
    if err != None:
        return {
                   'success': False,
                   'message': err
               }, 403

    # Get the account
    account = Account(account_id)

    # Check if account is cleared to execute
    executeError = account.canExecute()

    if executeError != None:
        return {
                   'success': False,
                   'message': executeError
               }, 403

    # Check if user has permissions to access account
    permissionError = account.hasPermissions(self.CurrentUserID())

    if permissionError != None:
        return {
                   'success': False,
                   'message': permissionError
               }, 403

    # Get account users
    account_users_query, err = account.getUsers(offset, limit)

    if err != None:
        return {
                   'success': False,
                   'message': err
               }, 500

    data = []

    for user in account_users_query:
        data.append({
            "user_id": user.UserRoles.user_id,
            "first_name": user.Users.u_first_name,
            "last_name": user.Users.u_last_name,
            "phone_area": user.Users.u_phone_area,
            "phone_number": user.Users.u_phone_number,
            "role_id": user.UserRoles.role_id,
            "role_name": user.Roles.r_name,
            "is_suspended": user.UserRoles.ur_is_suspended,
            "is_active": user.UserRoles.ur_is_active
        })

    return {
               "success": True,
               "count": len(data),
               "offset": offset,
               "limit": limit,
               "users": data
           }, 200


def create_new_account(body=None):
    """create_new_account

    Create a new account. # noqa: E501

    :param body:
    :type body: dict | bytes

    :rtype: json
    """
    if connexion.request.is_json:
        body = AccountRequest.from_dict(connexion.request.get_json())
        err = CurrentUserCanExecute()
        if err != None:
            return {
                'success': False,
                'message': err
            }, 403

        if body.parent_id > 0:

            parentAccount = Account(body.parent_id)

            # Check if parent account is cleared to execute
            parentExecuteError = parentAccount.canExecute()

            if parentExecuteError != None:
                return {
                    'success': False,
                    'message': parentExecuteError
                }, 403

            # Check if user has permissions to access parent account
            permissionError = parentAccount.hasPermissions(self.CurrentUserID())

            if permissionError != None:
                return {
                    'success': False,
                    'message': permissionError
                }, 403

        accountID, err = Account().new(body, self.CurrentUserID())

        if err != None:
            return {
                'success': False,
                'message': err
            }, 500

        return {
            'success': True,
            'account_id': accountID,
            'message ': 'new account created successfully'
        }, 201

def suspend_user(body=None):
    """suspend_user

    update the account user suspend status. # noqa: E501

    :param body: 
    :type body: dict | bytes

    :rtype: json
    """
    if connexion.request.is_json:
        body = AccountUserSuspendRequest.from_dict(connexion.request.get_json())
        err = CurrentUserCanExecute()
        if err != None:
            return {
                       'success': False,
                       'message': err
                   }, 403

        account = Account(body.account_id)

        # Check if account is cleared to execute
        executeError = account.canExecute()

        if executeError != None:
            return {
                       'success': False,
                       'message': executeError
                   }, 403

        # Check if user has permissions to access account
        permissionError = account.hasPermissions(self.CurrentUserID())

        if permissionError != None:
            return {
                       'success': False,
                       'message': permissionError
                   }, 403

        err = account.suspendUser(body.user_id, body.role_id, body.suspend)

        if err != None:
            return {
                       'success': False,
                       'message': err
                   }, 500

        return {
                   'success': True,
                   'message ': 'account user suspend status updated successfully'
               }, 200
