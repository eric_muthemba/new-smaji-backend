import connexion
import six

from server.models.deployment import Deployment  # noqa: E501
from server.models.manufacturer import Manufacturer  # noqa: E501
from server.models.manufacturer_error import ManufacturerError  # noqa: E501
from server.models.manufacturer_response import ManufacturerResponse  # noqa: E501
from server.models.registration import Registration  # noqa: E501
from server.models.registration_error import RegistrationError  # noqa: E501
from server.models.registration_response import RegistrationResponse  # noqa: E501
from server import util


def api_data_apis_manufacturer_class_instance_get(manufacture_id):  # noqa: E501
    """api_data_apis_manufacturer_class_instance_get

    gets the list of all manufactures # noqa: E501

    :param manufacture_id: The id of a manufacture
    :type manufacture_id: int

    :rtype: None
    """
    return 'do some magic!'


def api_data_apis_manufacturer_class_instance_get_all():  # noqa: E501
    """api_data_apis_manufacturer_class_instance_get_all

    gets manufacture by id # noqa: E501


    :rtype: None
    """
    return 'do some magic!'


def api_data_apis_manufacturer_class_instance_post(body=None):  # noqa: E501
    """api_data_apis_manufacturer_class_instance_post

    Register&#x27;s a device manufacturer. This information will be used to track device origin. # noqa: E501

    :param body: 
    :type body: dict | bytes

    :rtype: ManufacturerResponse
    """
    if connexion.request.is_json:
        body = Manufacturer.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def api_device_apis_device_decommissioning_class_instance_delete(device_id):  # noqa: E501
    """api_device_apis_device_decommissioning_class_instance_delete

    decommisions a device from service # noqa: E501

    :param device_id: Device id of device to be decommisioned
    :type device_id: str

    :rtype: None
    """
    return 'do some magic!'


def api_device_apis_device_deployment_class_instance_post(body=None):  # noqa: E501
    """api_device_apis_device_deployment_class_instance_post

    assigns device to owner during deploying # noqa: E501

    :param body: 
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = Deployment.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def api_device_apis_device_inventory_class_instance_get():  # noqa: E501
    """api_device_apis_device_inventory_class_instance_get

    assigns device to owner during deploying # noqa: E501


    :rtype: None
    """
    return 'do some magic!'


def api_device_apis_device_registration_class_instance_get(device_id):  # noqa: E501
    """api_device_apis_device_registration_class_instance_get

    gets device by id # noqa: E501

    :param device_id: The id of a device
    :type device_id: str

    :rtype: None
    """
    return 'do some magic!'


def api_device_apis_device_registration_class_instance_get_all():  # noqa: E501
    """api_device_apis_device_registration_class_instance_get_all

    Adds a device to the system # noqa: E501


    :rtype: None
    """
    return 'do some magic!'


def api_device_apis_device_registration_class_instance_post(body=None):  # noqa: E501
    """api_device_apis_device_registration_class_instance_post

    Adds a device to the system # noqa: E501

    :param body: 
    :type body: dict | bytes

    :rtype: RegistrationResponse
    """
    if connexion.request.is_json:
        body = Registration.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'
