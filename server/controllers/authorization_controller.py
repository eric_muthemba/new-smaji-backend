from typing import List
import time
from jose import JWTError, jwt
import six
from werkzeug.exceptions import Unauthorized


JWT_ISSUER = 'eric@diasporaai.com'
JWT_SECRET = '*#bambushka**12345weRock!!!'
JWT_LIFETIME_SECONDS = 600
JWT_ALGORITHM = 'HS256'

def create_BearerAuth(user_id):
        timestamp = int(time.time())
        payload = {
            "iss": JWT_ISSUER,
            "iat": int(timestamp),
            #"exp": int(timestamp + JWT_LIFETIME_SECONDS),
            "sub": str(user_id),
        }
        return jwt.encode(payload, JWT_SECRET, algorithm=JWT_ALGORITHM)

def create_DeviceAuth(device_id):
    timestamp = int(time.time())
    payload = {
        "iss": JWT_ISSUER,
        "iat": int(timestamp),
        "sub": str(device_id),
    }
    return jwt.encode(payload, JWT_SECRET, algorithm=JWT_ALGORITHM)


def check_BearerAuth(token):
    try:
        return jwt.decode(token, JWT_SECRET, algorithms=[JWT_ALGORITHM])
    except JWTError as e:
        six.raise_from(Unauthorized, e)

def check_DeviceAuth(token):
    try:
        return jwt.decode(token, JWT_SECRET, algorithms=[JWT_ALGORITHM])
    except JWTError as e:
        six.raise_from(Unauthorized, e)

