import connexion
import six

from server.models.registration_day_sms import RegistrationDaySms  # noqa: E501
from server.models.sms_scheduled_to_runs_once import SmsScheduledToRunsOnce  # noqa: E501
from server.models.weekday_sms import WeekdaySms  # noqa: E501
from server import util


def api_data_apis_get_all_scheduled_jobs_class_instance_post():  # noqa: E501
    """api_data_apis_get_all_scheduled_jobs_class_instance_post

    trigers an sms to be sent on a specific day and time of the week # noqa: E501


    :rtype: None
    """
    return 'do some magic!'


def api_data_apis_registration_day_sms_class_instance_post(body=None):  # noqa: E501
    """api_data_apis_registration_day_sms_class_instance_post

    trigers registration day sms # noqa: E501

    :param body:
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = RegistrationDaySms.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def api_data_apis_sms_on_specfic_days_of_the_week_class_instance_post(body=None):  # noqa: E501
    """api_data_apis_sms_on_specfic_days_of_the_week_class_instance_post

    trigers an sms to be sent on a specific day and time of the week # noqa: E501

    :param body:
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = WeekdaySms.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def api_data_apis_sms_scheduled_to_runs_once_class_instance_post(body=None):  # noqa: E501
    """api_data_apis_sms_scheduled_to_runs_once_class_instance_post

    trigers an sms to be sent on a specified date and time # noqa: E501

    :param body: 
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = SmsScheduledToRunsOnce.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'
