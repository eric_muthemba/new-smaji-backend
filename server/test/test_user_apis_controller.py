# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from server.models.login_request import LoginRequest  # noqa: E501
from server.models.login_response import LoginResponse  # noqa: E501
from server.models.privileges import Privileges  # noqa: E501
from server.models.two_factor_authentication_response import TwoFactorAuthenticationResponse  # noqa: E501
from server.models.user import User  # noqa: E501
from server.models.user_response import UserResponse  # noqa: E501
from server.models.user_verify import UserVerify  # noqa: E501
from server.models.verify_response import VerifyResponse  # noqa: E501
from server.test import BaseTestCase


class TestUserAPIsController(BaseTestCase):
    """UserAPIsController integration test stubs"""

    def test_api_user_apis_user_admin_privileges_class_instance_patch(self):
        """Test case for api_user_apis_user_admin_privileges_class_instance_patch

        elevate privileges
        """
        body = Privileges()
        response = self.client.open(
            '/user/privileges',
            method='PATCH',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_user_apis_user_data_class_instance_get(self):
        """Test case for api_user_apis_user_data_class_instance_get

        Get user by user name
        """
        response = self.client.open(
            '/user/{username}'.format(username='username_example'),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_user_apis_user_deletion_class_instance_delete(self):
        """Test case for api_user_apis_user_deletion_class_instance_delete

        Delete user
        """
        response = self.client.open(
            '/user/{username}'.format(username='username_example'),
            method='DELETE')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_user_apis_user_login_class_instance_post(self):
        """Test case for api_user_apis_user_login_class_instance_post

        login
        """
        body = LoginRequest()
        response = self.client.open(
            '/user/login',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_user_apis_user_login_class_instance_verify(self):
        """Test case for api_user_apis_user_login_class_instance_verify

        login
        """
        body = UserVerify()
        response = self.client.open(
            '/user/2fa',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_user_apis_user_logout_class_instance_get(self):
        """Test case for api_user_apis_user_logout_class_instance_get

        Logs out current logged in user session
        """
        response = self.client.open(
            '/user/logout',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_user_apis_user_registration_class_instance_post(self):
        """Test case for api_user_apis_user_registration_class_instance_post

        Create user
        """
        body = User()
        response = self.client.open(
            '/user/registration',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_user_apis_user_registration_class_instance_verify(self):
        """Test case for api_user_apis_user_registration_class_instance_verify

        
        """
        body = UserVerify()
        response = self.client.open(
            '/user/verify',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_user_apis_user_update_class_instance_put(self):
        """Test case for api_user_apis_user_update_class_instance_put

        Updated user
        """
        body = User()
        response = self.client.open(
            '/user/{username}'.format(username='username_example'),
            method='PUT',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
