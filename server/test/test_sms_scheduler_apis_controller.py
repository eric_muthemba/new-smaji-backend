# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from server.models.registration_day_sms import RegistrationDaySms  # noqa: E501
from server.models.sms_scheduled_to_runs_once import SmsScheduledToRunsOnce  # noqa: E501
from server.models.weekday_sms import WeekdaySms  # noqa: E501
from server.test import BaseTestCase


class TestSmsSchedulerAPIsController(BaseTestCase):
    """SmsSchedulerAPIsController integration test stubs"""

    def test_api_data_apis_get_all_scheduled_jobs_class_instance_post(self):
        """Test case for api_data_apis_get_all_scheduled_jobs_class_instance_post

        
        """
        response = self.client.open(
            '/sms/get_all_jobs',
            method='POST')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_data_apis_registration_day_sms_class_instance_post(self):
        """Test case for api_data_apis_registration_day_sms_class_instance_post

        
        """
        body = RegistrationDaySms()
        response = self.client.open(
            '/sms/registration_day',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_data_apis_sms_on_specfic_days_of_the_week_class_instance_post(self):
        """Test case for api_data_apis_sms_on_specfic_days_of_the_week_class_instance_post

        
        """
        body = WeekdaySms()
        response = self.client.open(
            '/sms/weekday',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_data_apis_sms_scheduled_to_runs_once_class_instance_post(self):
        """Test case for api_data_apis_sms_scheduled_to_runs_once_class_instance_post

        
        """
        body = SmsScheduledToRunsOnce()
        response = self.client.open(
            '/sms/specific_date',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
