# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from server.models.group_request import GroupRequest  # noqa: E501
from server.models.group_user_activate_request import GroupUserActivateRequest  # noqa: E501
from server.models.group_user_delete_request import GroupUserDeleteRequest  # noqa: E501
from server.models.group_user_request import GroupUserRequest  # noqa: E501
from server.models.group_user_suspend_request import GroupUserSuspendRequest  # noqa: E501
from server.test import BaseTestCase


class TestGroupingAPIsController(BaseTestCase):
    """GroupingAPIsController integration test stubs"""

    def test_api_data_apis_group_class_instance_activate_user(self):
        """Test case for api_data_apis_group_class_instance_activate_user

        
        """
        body = GroupUserActivateRequest()
        response = self.client.open(
            '/group/user/activate',
            method='PUT',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_data_apis_group_class_instance_add_user(self):
        """Test case for api_data_apis_group_class_instance_add_user

        
        """
        body = GroupUserRequest()
        response = self.client.open(
            '/group/user',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_data_apis_group_class_instance_delete_user(self):
        """Test case for api_data_apis_group_class_instance_delete_user

        
        """
        body = GroupUserDeleteRequest()
        response = self.client.open(
            '/group/user/delete',
            method='PUT',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_data_apis_group_class_instance_get(self):
        """Test case for api_data_apis_group_class_instance_get

        
        """
        response = self.client.open(
            '/group/{group_id}'.format(group_id=56),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_data_apis_group_class_instance_get_all(self):
        """Test case for api_data_apis_group_class_instance_get_all

        
        """
        query_string = [('offset', 56),
                        ('limit', 56)]
        response = self.client.open(
            '/group/list',
            method='GET',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_data_apis_group_class_instance_get_users(self):
        """Test case for api_data_apis_group_class_instance_get_users

        
        """
        query_string = [('offset', 56),
                        ('limit', 56)]
        response = self.client.open(
            '/group/{group_id}/users'.format(group_id=56),
            method='GET',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_data_apis_group_class_instance_post(self):
        """Test case for api_data_apis_group_class_instance_post

        
        """
        body = GroupRequest()
        response = self.client.open(
            '/group',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_data_apis_group_class_instance_suspend_user(self):
        """Test case for api_data_apis_group_class_instance_suspend_user

        
        """
        body = GroupUserSuspendRequest()
        response = self.client.open(
            '/group/user/suspend',
            method='PUT',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
