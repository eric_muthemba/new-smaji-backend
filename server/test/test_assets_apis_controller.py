# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from server.models.privileges import Privileges  # noqa: E501
from server.models.tank_object import TankObject  # noqa: E501
from server.test import BaseTestCase


class TestAssetsAPIsController(BaseTestCase):
    """AssetsAPIsController integration test stubs"""

    def test_assets_apis_class_instance_asset_deletion(self):
        """Test case for assets_apis_class_instance_asset_deletion

        Delete specific asset
        """
        response = self.client.open(
            '/Asset/{asset_id}'.format(asset_id='asset_id_example'),
            method='DELETE')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_assets_apis_class_instance_asset_report(self):
        """Test case for assets_apis_class_instance_asset_report

        Report specific asset.
        """
        body = TankObject()
        response = self.client.open(
            '/Asset/{asset_type}'.format(asset_type='asset_type_example'),
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_assets_apis_class_instance_asset_report_image(self):
        """Test case for assets_apis_class_instance_asset_report_image

        uploads asset image
        """
        data = dict(asset_image='asset_image_example')
        response = self.client.open(
            '/Asset/{asset_id}/image'.format(asset_id='asset_id_example'),
            method='POST',
            data=data,
            content_type='multipart/form-data')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_assets_apis_class_instance_update_asset(self):
        """Test case for assets_apis_class_instance_update_asset

        updates specific asset
        """
        body = Privileges()
        response = self.client.open(
            '/Asset/update/{asset_id}'.format(asset_id='asset_id_example'),
            method='PATCH',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
