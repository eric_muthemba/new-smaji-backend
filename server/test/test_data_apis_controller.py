# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from server.models.push import Push  # noqa: E501
from server.test import BaseTestCase


class TestDataAPIsController(BaseTestCase):
    """DataAPIsController integration test stubs"""

    def test_api_data_apis_push_data_class_instance_post(self):
        """Test case for api_data_apis_push_data_class_instance_post

        
        """
        body = Push()
        response = self.client.open(
            '/data/push',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
