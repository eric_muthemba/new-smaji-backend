# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from server.models.deployment import Deployment  # noqa: E501
from server.models.manufacturer import Manufacturer  # noqa: E501
from server.models.manufacturer_error import ManufacturerError  # noqa: E501
from server.models.manufacturer_response import ManufacturerResponse  # noqa: E501
from server.models.registration import Registration  # noqa: E501
from server.models.registration_error import RegistrationError  # noqa: E501
from server.models.registration_response import RegistrationResponse  # noqa: E501
from server.test import BaseTestCase


class TestDeviceAPIsController(BaseTestCase):
    """DeviceAPIsController integration test stubs"""

    def test_api_data_apis_manufacturer_class_instance_get(self):
        """Test case for api_data_apis_manufacturer_class_instance_get

        
        """
        response = self.client.open(
            '/device/manufacturer/{manufacture_id}'.format(manufacture_id=56),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_data_apis_manufacturer_class_instance_get_all(self):
        """Test case for api_data_apis_manufacturer_class_instance_get_all

        
        """
        response = self.client.open(
            '/device/manufacturer/list',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_data_apis_manufacturer_class_instance_post(self):
        """Test case for api_data_apis_manufacturer_class_instance_post

        
        """
        body = Manufacturer()
        response = self.client.open(
            '/device/manufacturer/supply',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_device_apis_device_decommissioning_class_instance_delete(self):
        """Test case for api_device_apis_device_decommissioning_class_instance_delete

        
        """
        query_string = [('device_id', 'device_id_example')]
        response = self.client.open(
            '/device/decommissioning',
            method='DELETE',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_device_apis_device_deployment_class_instance_post(self):
        """Test case for api_device_apis_device_deployment_class_instance_post

        
        """
        body = Deployment()
        response = self.client.open(
            '/device/deployment',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_device_apis_device_inventory_class_instance_get(self):
        """Test case for api_device_apis_device_inventory_class_instance_get

        
        """
        response = self.client.open(
            '/device/inventory',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_device_apis_device_registration_class_instance_get(self):
        """Test case for api_device_apis_device_registration_class_instance_get

        
        """
        response = self.client.open(
            '/device/{device_id}'.format(device_id='device_id_example'),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_device_apis_device_registration_class_instance_get_all(self):
        """Test case for api_device_apis_device_registration_class_instance_get_all

        
        """
        response = self.client.open(
            '/device/list',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_device_apis_device_registration_class_instance_post(self):
        """Test case for api_device_apis_device_registration_class_instance_post

        
        """
        body = Registration()
        response = self.client.open(
            '/device/registration',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
