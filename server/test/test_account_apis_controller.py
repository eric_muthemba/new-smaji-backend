# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from server.models.account_request import AccountRequest  # noqa: E501
from server.models.account_user_activate_request import AccountUserActivateRequest  # noqa: E501
from server.models.account_user_delete_request import AccountUserDeleteRequest  # noqa: E501
from server.models.account_user_request import AccountUserRequest  # noqa: E501
from server.models.account_user_suspend_request import AccountUserSuspendRequest  # noqa: E501
from server.test import BaseTestCase


class TestAccountAPIsController(BaseTestCase):
    """AccountAPIsController integration test stubs"""

    def test_api_data_apis_account_class_instance_activate_user(self):
        """Test case for api_data_apis_account_class_instance_activate_user

        
        """
        body = AccountUserActivateRequest()
        response = self.client.open(
            '/account/user/activate',
            method='PUT',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_data_apis_account_class_instance_add_user(self):
        """Test case for api_data_apis_account_class_instance_add_user

        
        """
        body = AccountUserRequest()
        response = self.client.open(
            '/account/user',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_data_apis_account_class_instance_delete_user(self):
        """Test case for api_data_apis_account_class_instance_delete_user

        
        """
        body = AccountUserDeleteRequest()
        response = self.client.open(
            '/account/user/delete',
            method='PUT',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_data_apis_account_class_instance_get(self):
        """Test case for api_data_apis_account_class_instance_get

        
        """
        response = self.client.open(
            '/account/{account_id}'.format(account_id=56),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_data_apis_account_class_instance_get_all(self):
        """Test case for api_data_apis_account_class_instance_get_all

        
        """
        query_string = [('offset', 56),
                        ('limit', 56)]
        response = self.client.open(
            '/account/list',
            method='GET',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_data_apis_account_class_instance_get_users(self):
        """Test case for api_data_apis_account_class_instance_get_users

        
        """
        query_string = [('offset', 56),
                        ('limit', 56)]
        response = self.client.open(
            '/account/{account_id}/users'.format(account_id=56),
            method='GET',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_data_apis_account_class_instance_post(self):
        """Test case for api_data_apis_account_class_instance_post

        
        """
        body = AccountRequest()
        response = self.client.open(
            '/account',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_api_data_apis_account_class_instance_suspend_user(self):
        """Test case for api_data_apis_account_class_instance_suspend_user

        
        """
        body = AccountUserSuspendRequest()
        response = self.client.open(
            '/account/user/suspend',
            method='PUT',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()


