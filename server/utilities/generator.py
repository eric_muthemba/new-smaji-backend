from random import seed
from random import randint
import uuid
import time

seed(int(time.time()))

def GenerateID():
    # Generates a random integer id
    return randint(1, 100000000)

def GenerateCode():
    return randint(1000, 9999)

def GenerateUUID():
    return str(uuid.uuid4())