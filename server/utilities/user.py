from sqlalchemy import exc
import re
from json import (dumps, loads)
from flask import g
import redis
from server.utilities.db_model import AlchemyEncoder, Users
from server.utilities import postgres,redis
import server.utilities.postgres
import json

def CurrentUser():
    if 'sj_current_user' in g:
        return g.get('sj_current_user')

    return None

def CurrentUserCanExecute():
    """
    Checks if the user has valid status

    Status: 
    - is_active: True
    - is_suspended: False

    Return
    -------
    str
        Error message explaining the failure
    """

    if CurrentUser() == None:
        return 'no current user set'
    
    if CurrentUser()['is_active'] == False: 
        return 'user account is deactivated'

    if CurrentUser()['is_suspended']: 
        return 'user account is currently suspended'

    return None

class User:
    """A class used to represent a person

    Attributes
    ----------
    user_id : str, optional
        The id of the user

    Methods
    -------
    get()
        Get a user from the database by ID
    validPhoneNumber(phone_area_code, phone_number)
        Prepare the provided phone area code and number to construct a valid phone number prefixed with a plus (+)
    phoneNumber()
        Return user phone number from the database
    """

    def __init__(self, user_id=None):
        """
        Parameters
        ----------
        user_id : str, optional
            The id of the user
        """

        self.id = user_id

        if user_id != None:
            self.query, err = self.get()
            if err != None:
                print('initialize user error:', err)
        else:
            self.query = None

    def get(self):
        """Get a user from the database by ID

        Return
        ------
        query: object
            Result from the database query (Users) or None
        error: str
            Error that may have occurred or None
        """

        if self.id == None:
            return None, 'user id is required'
        
        try:
            user_query = postgres.session.query(Users).filter(Users.id == self.id, Users.is_deleted == False).first()
        except exc.SQLAlchemyError as e:
            print('error getting user:', e)
            return None, 'error getting user'
        else:
            if user_query:
                return user_query, None
            else:
                return None, 'user not found'
        finally:
            postgres.session.close()

    def canExecute(self):
        """Check the user status and validity to execute an action in the system

        Return
        ------
        str
            message describing the failed status check or None

        """

        if self.query == None:
            return 'account not found'

        if self.query.is_active == False:
            return 'user is not active'

        if self.query.is_suspended:
            return 'user is suspended'

        return None

    def __removeNonNumericCharacters(self, value):
        return re.sub("[^0-9]", "", value)

    def __removeZeroFromStartOfValue(self, value):

        if value.startswith('0', 0, len(value)):
            return self.removeNonNumericCharacters(value[1:])

        return value

    def validPhoneNumber(self, phone_area_code, phone_number):
        """Prepare the provided phone area code and number to construct a valid phone number prefixed with a plus (+)

        Parameters
        ----------
        phone_area_code : str
            The phone area code only without any non numeric characters
        phone_number : str
            The phone number only without any non numeric characters

        Returns
        -------
        string
            valid phone number with a plus (+) prefix or None
        """

        value = self.__removeNonNumericCharacters(phone_area_code + self.__removeZeroFromStartOfValue(phone_number)) 

        if value == '':
            return None

        return '+' + value

    def phoneNumber(self):
        """Return user phone number from the database

        Returns
        -------
        string
            valid phone number with a plus (+) prefix or None
        """

        if self.query == None:
            return None

        return self.validPhoneNumber(self.query.u_phone_area, self.query.u_phone_number)

class AuthorizedUser(User):
    """
        The authenticated and authorized user accessing the system at the moment

        Attributes
        ----------
        user_id : str, optional
            The id of the user

        Methods
        -------
        getAndSetUser()
            Gets the user currently making the request from the database or caches. If user is not in cache it caches the user.
        cacheUser()
            Saves the user to the system cache for future quick access
        getFromCache()
            Gets the current user from cache or returns None
    """

    def __init__(self, user_id):
        super().__init__(user_id)
        self.current = None
        self.getAndSetUser()

    def getAndSetUser(self):
        """Gets the user currently making the request from the database or caches. If user is not in cache it caches the user.
        """

        # Check if user cached
        cacheUser = self.getFromCache(self.id)

        if cacheUser != None:
            self.current = cacheUser
        else:

            query, err = self.get()  

            if err != None:
                raise Exception(err)

            jsonUser = json.dumps(query, cls=AlchemyEncoder)
            self.current = loads(jsonUser) # Convert to dictionary

            self.cacheUser(jsonUser)

    def cacheUser(self, user):
        """saves the user to cache for quick access

        Return
        -------
        boolean
            When successful it returns true and false when it fails
        """

        try:

            redis.connection.set('current_user_' + self.id, user, ex=3600)

        except redis.RedisError as e:

            print('redis error, cache current user: ', e)

            return False
        else:           
            return True

        return None     
        
    def getFromCache(self, user_id):

        """Check if the user exists in the cache

        Return
        -------
        Dict
            If the user exists in cache return object else None
        """

        try:

            value = redis.connection.get('current_user_' + user_id)

            if value != None:
                return loads(value)

        except redis.RedisError as e:

            print('redis error, get cached current user: ', e)

        return None

class Role(User):

    def __init__(self, user_id):

        super().__init__(user_id)

        self.query = self.get()

    