# coding: utf-8

from sqlalchemy.dialects.postgresql import TIMESTAMP
from sqlalchemy import create_engine,Boolean, Column, DateTime, ForeignKey, Integer, Float, String, Text, text, UniqueConstraint, PrimaryKeyConstraint
from sqlalchemy.orm import scoped_session,sessionmaker,relationship
from sqlalchemy.ext.declarative import declarative_base, DeclarativeMeta
import json

Base = declarative_base()
metadata = Base.metadata

def init_db(uri):
    engine = create_engine(uri, convert_unicode=True)
    db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))
    Base.query = db_session.query_property()
    metadata.create_all(bind=engine)

    return db_session

Base = declarative_base()
metadata = Base.metadata

class AlchemyEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj.__class__, DeclarativeMeta):
            # an SQLAlchemy class
            fields = {}
            for field in [x for x in dir(obj) if not x.startswith('_') and x != 'metadata']:
                data = obj.__getattribute__(field)
                try:
                    json.dumps(data) # this will fail on non-encodable values, like other classes
                    fields[field] = data
                except TypeError:
                    fields[field] = None
            # a json-encodable dict
            return fields

        return json.JSONEncoder.default(self, obj)

class Users(Base):

    __tablename__ = 'users'

    id = Column(String(50), primary_key=True)
    u_first_name = Column(String(50), nullable=False)
    u_middle_name = Column(String(50), nullable=True)
    u_last_name = Column(String(50), nullable=False)
    u_password = Column(String(100), nullable=False)
    u_last_password_changed_on = Column(Integer, nullable=True)
    u_password_change_count = Column(Integer, nullable=True)
    u_phone_area = Column(String(10), index=True, nullable=True)
    u_phone_number = Column(String(100), index=True, nullable=True)
    u_email = Column(String(100), unique=True, nullable=True)
    u_gender = Column(Integer, nullable=True)
    u_birthday = Column(Integer, nullable=True)
    u_description = Column(String(200), nullable=True)
    u_country_code = Column(String(2), nullable=True)
    u_location_code = Column(String(3), nullable=True)
    u_address = Column(String(50), nullable=True)
    u_profile_photo = Column(String(100), unique=True, nullable=True)
    u_enable_two_factor = Column(Boolean, index=True, default=False)
    u_phone_number_verified = Column(Boolean, index=True, default=False)
    u_email_verified = Column(Boolean, index=True, default=False)
    u_identity_verified = Column(Boolean, index=True, default=False)
    u_last_login = Column(Integer, index=True, nullable=True)
    u_last_seen = Column(Integer, index=True, nullable=True)
    is_suspended = Column(Boolean, index=True, default=False)
    is_active = Column(Boolean, index=True, default=True)
    is_deactivated_on = Column(Integer, index=True, nullable=True)
    is_deleted = Column(Boolean, index=True, default=False)
    is_deleted_on = Column(Integer, index=True, nullable=True)
    created_on = Column(Integer, nullable=False)
    modified_on = Column(Integer, nullable=False)

    __table_args__ = (
        UniqueConstraint('u_phone_area', 'u_phone_number', name='unique_phone_number'),
    )

class UserRoles(Base):

    __tablename__ = 'user_roles'

    user_id = Column(String(50), nullable=False)
    role_id = Column(Integer, nullable=False)
    account_id = Column(Integer, nullable=True)
    group_id = Column(Integer, nullable=True)
    ur_is_suspended = Column(Boolean, index=True, default=False)
    ur_is_active = Column(Boolean, index=True, default=True)
    ur_is_deactivated_on = Column(Integer, index=True, nullable=True)
    ur_is_deleted = Column(Boolean, index=True, default=False)
    ur_is_deleted_on = Column(Integer, index=True, nullable=True)
    created_on = Column(Integer, nullable=False)
    modified_on = Column(Integer, nullable=False)

    __table_args__ = (
        PrimaryKeyConstraint('user_id', 'role_id', 'account_id', 'group_id', name='pk_user_id'),
        UniqueConstraint('user_id', 'role_id', 'account_id', 'group_id', name='unique_user_id'),
    )

class Roles(Base):

    __tablename__ = 'roles'

    id = Column(Integer, primary_key=True)
    r_account_id = Column(Integer, nullable=True)
    r_group_id = Column(Integer, nullable=True)
    r_name = Column(String(50), nullable=False)
    r_description = Column(String(200), nullable=False)
    r_is_active = Column(Boolean, index=True, default=True)
    r_is_deactivated_on = Column(Integer, index=True, nullable=True)
    r_is_deleted = Column(Boolean, index=True, default=False)
    r_is_deleted_on = Column(Integer, index=True, nullable=True)
    created_by = Column(String(50), nullable=False)
    created_on = Column(Integer, nullable=False)
    modified_on = Column(Integer, nullable=False)

    __table_args__ = (
        UniqueConstraint('r_account_id', 'r_group_id', 'r_name', name='unique_role_name'),
    )

class Permission(Base):

    __tablename__ = 'permission'

    id = Column(Integer, primary_key=True)
    grouping = Column(String(50), index=True, nullable=False)
    code = Column(String(50), unique=True, nullable=False)
    entity_name = Column(String(50), nullable=True)
    action_name = Column(String(50), nullable=True)
    can_maker_checker = Column(Boolean, index=True, default=False)
    is_active = Column(Boolean, index=True, default=True)
    type_enum = Column(Integer, index=True, nullable=False)

class RolePermission(Base):

    __tablename__ = 'role_permission'

    role_id = Column(Integer, primary_key=True)
    permission_id = Column(Integer, index=True, nullable=False)

class Accounts(Base):
    __tablename__ = 'accounts'

    id = Column(Integer, primary_key=True)
    a_parent_id = Column(Integer, index=True, nullable=True)
    a_name = Column(String(50), unique=True, nullable=False)
    a_description = Column(String(200), nullable=True)
    a_initial_billing_grace_period_days = Column(Integer, nullable=True)
    a_billable_since = Column(Integer, nullable=True)
    is_billable = Column(Boolean, index=True, default=False)
    is_suspended = Column(Boolean, index=True, default=False)
    is_active = Column(Boolean, index=True, default=True)
    is_deleted = Column(Boolean, index=True, default=False)
    is_deleted_on = Column(Integer, index=True, nullable=True)
    created_by = Column(String(50), index=True, nullable=False)
    created_on = Column(Integer, nullable=False)
    modified_on = Column(Integer, nullable=False)

class Groups(Base):
    __tablename__ = 'groups'

    id = Column(Integer, primary_key=True)
    g_account_id = Column(Integer, index=True, nullable=True)
    g_parent_id = Column(Integer, index=True, nullable=True)
    g_name = Column(String(50), unique=True, nullable=False)
    g_description = Column(String(200), nullable=True)
    g_device_count = Column(Integer, nullable=True)
    g_tank_count = Column(Integer, nullable=True)
    g_is_suspended = Column(Boolean, index=True, default=False)
    g_is_active = Column(Boolean, index=True, default=True)
    g_is_deleted = Column(Boolean, index=True, default=False)
    g_is_deleted_on = Column(Integer, index=True, nullable=True)
    g_created_by = Column(String(50), index=True, nullable=False)
    created_on = Column(Integer, nullable=False)
    modified_on = Column(Integer, nullable=False)

class Manufacturer(Base):
    __tablename__ = 'manufacturers'

    id = Column(Integer, primary_key=True)
    m_registered_name = Column(String(50), unique=True, nullable=True)
    m_business_name = Column(String(50), unique=True, nullable=False)
    m_business_number = Column(String(50), unique=True, nullable=True)
    m_description = Column(String(200), nullable=True)
    m_country = Column(String(2), index=True, nullable=True)
    m_city = Column(String(3), index=True, nullable=True)
    m_address = Column(String(50), nullable=True)
    is_suspended = Column(Boolean, index=True, default=False)
    is_active = Column(Boolean, index=True, default=True)
    is_deleted = Column(Boolean, index=True, default=False)
    created_on = Column(Integer, nullable=False)
    modified_on = Column(Integer, nullable=False)

class Client(Base):
    __tablename__ = 'clients'

    user_name = Column(String(50), nullable=False)
    phone_number = Column(String(50), nullable=False)
    email = Column(String(50), nullable=False)
    type = Column(String(50), nullable=False)
    password = Column(String(100), nullable=False)
    confirmed = Column(Boolean, nullable=False)
    country = Column(String(20), nullable=False)
    uuid = Column(String(50), primary_key=True)

class Device(Base):
    __tablename__ = 'devices'

    device_id = Column(String(50), primary_key=True)
    manufacturer_id = Column(Integer, index=True, nullable=False)
    group_id = Column(Integer, index=True, nullable=True)
    client_id = Column(Integer, index=True, nullable=True)
    tank_id = Column(Integer, index=True, nullable=True)
    d_name = Column(String(50), unique=True, nullable=False)
    d_description = Column(String(50), nullable=True)
    d_make = Column(String(50), nullable=False)
    d_model = Column(String(50), nullable=False)
    d_serial_number = Column(String(50), unique=True, nullable=False)
    d_imei_number = Column(String(50), unique=True, nullable=True)
    d_sim_number = Column(String(50), unique=True, nullable=True)
    communication_protocol = Column(String(50), nullable=True)
    d_bought_on = Column(Integer, nullable=False)
    submitted_on = Column(Integer, nullable=False)
    submitted_by = Column(String(50), nullable=False)
    approved_on = Column(Integer, nullable=False)
    approved_by = Column(String(50), nullable=False)
    fitted_on = Column(Integer, index=True, nullable=True)
    fitted_by = Column(String(50), nullable=True)
    activated_on = Column(Integer, nullable=True)
    activated_by = Column(String(50), nullable=True)
    deactivated_on = Column(Integer, nullable=True)
    deactivated_by = Column(String(50), nullable=True)
    d_sensor_offset = Column(Integer, nullable=True)
    d_capacity_percentage = Column(Integer, index=True, nullable=True)
    d_alert_capacity_percentage = Column(Integer, nullable=True)
    d_empty_alert_sent = Column(Boolean, default=False) 
    d_low_alert_sent = Column(Boolean, default=False) 
    d_half_alert_sent = Column(Boolean, default=False)  
    d_full_alert_sent = Column(Boolean, default=False)
    last_full_capacity_on = Column(Integer, nullable=True)
    last_empty_capacity_on = Column(Integer, nullable=True)
    last_notification_on = Column(Integer, nullable=True)
    last_seen_on = Column(Integer, index=True, nullable=True)
    last_height_cm = Column(Float, nullable=True)
    last_battery_mv = Column(Float, nullable=True)
    last_temperature_celsius = Column(Float, nullable=True)
    last_signal = Column(Float, nullable=True)
    last_humidity_percentage = Column(Float, nullable=True)
    is_suspended = Column(Boolean, index=True, default=False)
    is_active = Column(Boolean, index=True, default=False)
    is_deleted = Column(Boolean, index=True, default=False)
    is_deleted_by = Column(String(50), nullable=True)
    created_on = Column(Integer, nullable=False)
    modified_on = Column(Integer, nullable=False)

class DeviceData(Base):
    __tablename__ = 'device_data'

    id = Column(String(50), primary_key=True)
    device_id = Column(String(50), index=True, nullable=False)
    tank_id = Column(Integer, index=True, nullable=False)
    dd_data_time = Column(Integer, nullable=False)
    dd_raw_height_cm = Column(Integer, nullable=False)
    dd_processed_height_cm = Column(Integer, nullable=True)
    dd_battery_mv = Column(Float, nullable=True)
    dd_raw_temperature_celsius = Column(Float, nullable=True)
    dd_processed_temperature_celsius = Column(Float, nullable=True)
    dd_signal = Column(Float, nullable=True)
    dd_raw_humidity_percentage = Column(Boolean, nullable=True)
    dd_processed_humidity_percentage = Column(Boolean, nullable=True)
    dd_device_status = Column(Integer, index=True, nullable=True)
    is_deleted = Column(Boolean, index=True, default=False)
    created_on = Column(Integer, nullable=False)
    modified_on = Column(Integer, nullable=False)
    
class Alert(Base):
    __tablename__ = 'alerts'

    clients_uuid = Column(ForeignKey(u'clients.uuid'), nullable=False)
    alerts_type = Column(String(50), nullable=False)
    timestamp = Column(DateTime, nullable=False)
    alert_message = Column(Text, nullable=False)
    id = Column(Integer, primary_key=True, server_default=text("nextval('alerts_id_seq'::regclass)"))

    client = relationship(u'Client')

class Deployment(Base):
    __tablename__ = 'deployment'

    id = Column(String(50), primary_key=True)
    device_id = Column(String(50), index=True, nullable=False)
    tank_id = Column(Integer, index=True, nullable=False)
    submitted_on = Column(Integer, nullable=True)
    submitted_by = Column(Integer, nullable=True)
    approved_on = Column(Integer, nullable=True)
    approved_by = Column(Integer, nullable=True)
    activated_on = Column(Integer, nullable=True)
    activated_by = Column(Integer, nullable=True) 
    deployed_on = Column(Integer, nullable=False)
    deployed_by = Column(Integer, nullable=False)
    decommissioned_on = Column(Integer, nullable=True)
    decommissioned_by = Column(Integer, nullable=True)   
    first_signal_on = Column(Integer, nullable=True)  
    is_suspended = Column(Boolean, index=True, default=False)
    is_active = Column(Boolean, index=True, default=True)
    is_deleted = Column(Boolean, index=True, default=False)
    created_on = Column(Integer, nullable=False)
    modified_on = Column(Integer, nullable=False)   

    __table_args__ = (
        UniqueConstraint('device_id', 'tank_id', 'is_active', name='unique_active'),
    )

class Log(Base):
    __tablename__ = 'logs'

    clients_uuid = Column(ForeignKey(u'clients.uuid'), nullable=False)
    ip_address = Column(String(20), nullable=False)
    timestamp = Column(DateTime, nullable=False)
    type = Column(String(20), nullable=False)
    message = Column(Text, nullable=False)
    id = Column(Integer, primary_key=True, server_default=text("nextval('logs_id_seq'::regclass)"))

class Maintenance(Base):
    __tablename__ = 'maintenance'

    devices_device_id = Column(ForeignKey(u'devices.device_id'), nullable=False)
    last_data_update = Column(DateTime, nullable=False)
    maintenance_date = Column(DateTime)
    id = Column(Integer, primary_key=True)

class Tanks(Base):
    __tablename__ = 'tanks'

    id = Column(Integer, primary_key=True)
    external_id = Column(String(50), nullable=True)
    group_id = Column(Integer, index=True, nullable=True)
    account_id = Column(Integer, index=True, nullable=False)
    t_name = Column(String(50), index=True, nullable=False)
    t_description = Column(String(200), nullable=True)
    t_make_model = Column(Integer, index=True, nullable=True)
    t_serial_number = Column(String(50), index=True, nullable=True)
    t_manufacture_date = Column(Integer, nullable=True)
    t_installation_date = Column(Integer, index=True, nullable=True)
    t_shape = Column(Integer, index=True, nullable=False)
    t_material = Column(Integer, nullable=True)
    t_color = Column(String(50), nullable=True)
    t_placement_position = Column(Integer, nullable=False)
    t_height_from_ground_cm = Column(Integer, nullable=True)
    t_height_cm = Column(Integer, nullable=True)    
    t_inlet_height_cm = Column(Integer, nullable=True)
    t_outlet_height_cm = Column(Integer, nullable=True)
    t_diameter_cm = Column(Integer, nullable=True)
    t_length_cm = Column(Integer, nullable=True)
    t_width_cm = Column(Integer, nullable=True)
    t_weight_empty_kg = Column(Integer, nullable=True)
    t_weight_full_kg = Column(Integer, nullable=True)
    t_capacity_liters = Column(Integer, nullable=False)
    t_water_source = Column(Integer, nullable=True)
    t_piping_type = Column(Integer, nullable=True)
    t_country = Column(String(2), index=True, nullable=True)
    t_location = Column(String(3), index=True, nullable=True)
    t_address = Column(String(50), nullable=True)
    t_geographic_coordinates_dd = Column(String(50), nullable=True)
    submitted_on = Column(Integer, nullable=False)
    submitted_by = Column(String(50), nullable=False)
    approved_on = Column(Integer, nullable=True)
    approved_by = Column(String(50), nullable=True)
    last_audited_on = Column(Integer, nullable=True)
    last_audited_by = Column(String(50), nullable=True)
    last_maintained_on = Column(Integer, nullable=True)
    last_maintained_by = Column(String(50), nullable=True)
    last_activated_on = Column(Integer, nullable=True)
    last_activated_by = Column(String(50), nullable=True)
    last_deactivated_on = Column(Integer, nullable=True)
    last_deactivated_by = Column(String(50), nullable=True)
    is_suspended = Column(Boolean, index=True, default=False)
    is_active = Column(Boolean, index=True, default=True)
    is_deleted = Column(Boolean, index=True, default=False)
    created_on = Column(Integer, nullable=False)
    modified_on = Column(Integer, nullable=False)

    __table_args__ = (
        UniqueConstraint('account_id', 't_name', name='unique_tank_name'),
        UniqueConstraint('t_make_model', 't_serial_number', name='unique_tank_serial_number'),
    )
    
class TankSpecification(Base):
    __tablename__ = 'tank_specification'

    id = Column(Integer, primary_key=True)
    group_id = Column(Integer, index=True, nullable=True)
    ts_make = Column(String(50), nullable=False)
    ts_model = Column(String(50), nullable=False)
    ts_description = Column(String(200), nullable=True)
    ts_manufacture_start_date = Column(Integer, nullable=True)
    ts_manufacture_end_date = Column(Integer, nullable=True)
    ts_shape = Column(Integer, nullable=False)
    ts_height_cm = Column(Integer, nullable=False)    
    ts_inlet_height_cm = Column(Integer, nullable=False)
    ts_outlet_height_cm = Column(Integer, nullable=True)
    ts_diameter_cm = Column(Integer, nullable=True)
    ts_length_cm = Column(Integer, nullable=True)
    ts_width_cm = Column(Integer, nullable=True)
    ts_weight_empty_kg = Column(Integer, nullable=True)
    ts_weight_full_kg = Column(Integer, nullable=True)
    ts_capacity_liters = Column(Integer, nullable=False)
    submitted_on = Column(Integer, nullable=False)
    submitted_by = Column(Integer, nullable=False)
    approved_on = Column(Integer, nullable=True)
    approved_by = Column(Integer, nullable=True)
    is_suspended = Column(Boolean, index=True, default=False)
    is_active = Column(Boolean, index=True, default=True)
    is_deleted = Column(Boolean, index=True, default=False)
    created_on = Column(Integer, nullable=False)
    modified_on = Column(Integer, nullable=False)

    __table_args__ = (
        UniqueConstraint('ts_make', 'ts_model', name='unique_make_model'),
    )
   